package com.scala.tesco.mewbase.clientNats

import java.math.BigInteger
import java.net.URI
import java.util
import java.util.concurrent.atomic.AtomicInteger
import java.util._
import java.util.concurrent.{TimeUnit, _}
import javax.security.auth.Subject

import io.netty.bootstrap.Bootstrap
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioSocketChannel
import io.netty.channel._
import nats.NatsException
import nats.codec._
//import org.nats._
import nats.client._


import nats.codec.{ClientFrame, ClientPublishFrame}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration.TimeUnit



//import com.scala.tesco.mewbase.nats._
/**
  * Created by Ricardo Martins on 29/08/2017.
  */
object MewClientNats {

  def main(args: Array[String]): Unit = {


    /* val c = new MewClientNats//(pprops, (o: Object) => {println("Connected")}, (o : Object) => {println("Disconnected")})
c.connect()
    this.synchronized(wait(2500))
    c.subscribe("hello", (msg:Message)=>{ c.onMessage(msg)})
    this.sy8nchronized(wait(2500))
    c.publish("hello", "body")
    this.synchronized(wait(2500))
    val req: Request = c.request("hello", "body", 1000, TimeUnit.SECONDS , 20, (msg:Message) => {c.onMessage(msg)})

    while(true){}
    // c.close()
  }*/
    val connector: NatsConnector = new NatsConnector
    connector.addHost("nats://47.91.89.84:4222")
    val client = new MewClientNats(connector)
    client.connect()
    this.synchronized(wait(2500))
    println(s"Is Connected: ${client.isConnected}")
   client.subscribe("hello", (msg) => {println(msg)})
    this.synchronized(wait(2500))
    client.publish("hello", "body")
    this.synchronized(wait(2500))
    client.close()

  }
}
class MewClientNats(connector: NatsConnector) extends Nats {

  val LOGGER = LoggerFactory.getLogger(classOf[MewClientNats])
  var shutDownEventLoop = connector.getEventLoopGroup == null
  var eventLoopGroup = if(shutDownEventLoop){new NioEventLoopGroup()}else{connector.getEventLoopGroup}

  /**
    * The current Netty {@link Channel} used for communicating with the NATS server. This field should never be null
    * after the constructor has finished.
    */
  // Must hold monitor #lock to access
  var channel:Channel = null
  // Must hold monitor #lock to access
  var closed = false
  var lock:Object = new Object
  var serverReady = false

  // Configuration values
  var automaticReconnect = connector.isAutomaticReconnect
  var idleTimeout = connector.getIdleTimeout
  var reconnectTimeWait = connector.getReconnectWaitTime
  var pedantic = connector.isPedantic
  var maxFrameSize = connector.getMaxFrameSize
  var pingInterval = connector.getPingInterval
  val serverList = new ServerList()


connector.getHosts.forEach((u:URI)=> serverList.addServer(u))
  //serverList.addServer(i)

  /**
    * Holds the publish commands that have been queued up due to the connection being down.
    *
    * Must hold monitor #lock to access this queue.
    */
  private val publishQueue: util.Queue[ClientPublishFrame] = new util.LinkedList[ClientPublishFrame]
  /**
    * Holds the list of subscriptions held by this {@code Nats} instance.
    *
    * Must hold monitor #lock to access.
    */
 val subscriptions = new util.HashMap[String, NatsSubscription]
  val listeners = new util.ArrayList[ConnectionStateListener]
listeners.addAll(connector.getListeners)
  /**
    * Counter used for obtaining subscription ids. Each subscription must have its own unique id that is sent to the
    * NATS server to uniquely identify each subscription..
    */
  val subscriptionId = new AtomicInteger
  var executor = connector.getCallbackExecutor


  /////Create INBOX
  def createInbox(): String ={
    val bytes: Array[Byte] = new Array[Byte](16)
    ThreadLocalRandom.current().nextBytes(bytes)
    val s:String = s"_INBOX. ${new BigInteger(bytes).abs()}"
    s
  }

  //Connect to servers
  def connect(): Unit = lock.synchronized{
    if(closed){

    }else{
      val server:ServerList#Server = serverList.nextServer
      LOGGER.debug(s"Attempting to connect ${server.getAddress} with user ${server.getUser}")
      new Bootstrap()
        .group(eventLoopGroup)
        .remoteAddress(server.getAddress)
        .channel(new NioSocketChannel().getClass)
        .handler(new NatsChannelInitializer(this))
        .connect().addListener(new ChannelFutureListener {

        override def operationComplete(future: ChannelFuture): Unit ={
          if(future.isSuccess){
            LOGGER.info(s"Connection to ${server.getAddress}")
            server.connectionSuccess()
            lock.synchronized{
              if(closed)future.channel().close()
            }
          }else{
            LOGGER.warn(s"Connection to ${server.getAddress} failed")
            server.connectionFailure()
            scheduleReconnect()
          }
        }
      })
    }
  }

    //ScheduleReconnect
   def scheduleReconnect() = lock synchronized{
    serverReady = false
    if (!closed && automaticReconnect) eventLoopGroup.next.schedule(new Runnable() {
      override def run(): Unit = {
        connect()
      }
    }, reconnectTimeWait, TimeUnit.MILLISECONDS)
  }

  ////////////
  /// Override Methods
  ///////////
  override def isConnected: Boolean = lock synchronized {
    channel != null && channel.isActive
  }

  override def close(): Unit = lock synchronized{
    closed = true
    serverReady = false
    if (channel != null) {
      channel.close
    }
    if (shutDownEventLoop) {
     eventLoopGroup.shutdownGracefully
    }
  // We have to make a copy of the subscriptions because calling subscription.close() modifies the subscriptions map.
    val subscriptionsCopy: Collection[Subscription] = new ArrayList[Subscription](subscriptions.values)

    subscriptionsCopy.forEach((s:Subscription)=> s.close())
   /* for (subscription:Subscription <- subscriptionsCopy) {
      subscription.close()
    }*/
    closed
  }

  override def isClosed: Boolean = lock synchronized{
    closed
  }
  ////////////////
  ///// requests functions
  ////////////////

  override def request(subject: String, timeout: Long, timeUnit: TimeUnit, messageHandlers: MessageHandler*): Request = request(subject, "", timeout, timeUnit, messageHandlers(0))

  override def request(subject: String, message: String, timeout: Long, timeUnit: TimeUnit, messageHandlers: MessageHandler*): Request = request(subject, message, timeout, timeUnit, 0, messageHandlers(0))

  override def request(subject: String, message: String, timeout: Long, unit: TimeUnit, maxReplies: Integer, messageHandlers: MessageHandler*): Request = {
    assertNatsOpen()


    if (message == null) {
      throw new IllegalArgumentException("Message can NOT be null.")
    }
    val inbox: String = createInbox
    val subscription: Subscription = subscribe(inbox, maxReplies)
    for (handler <- messageHandlers) {
      subscription.addMessageHandler(handler)
    }
    val scheduler: ScheduledExecutorService = if ((channel == null)) {
      eventLoopGroup.next
    }
    else {
      channel.eventLoop
    }
    scheduler.schedule(new Runnable() {
      override def run(): Unit = {
        subscription.close()
      }
    }, timeout, unit)
    val publishFrame: ClientPublishFrame = new ClientPublishFrame(subject, message, inbox)
    publish(publishFrame)
    return new Request() {
      override def close(): Unit = {
        subscription.close()
      }

      override

      def getSubject: String = {
        subject
      }

      override

      def getReceivedReplies: Int = {
        subscription.getReceivedMessages
      }

      override

      def getMaxReplies: Integer = {
        maxReplies
      }
    }
  }


  ////////////////
  ///// subscribe functions
  ////////////////




  /*override def subscribe(subject: String, messageHandlers: MessageHandler*): Subscription = {

    subscribe(subject, null, null, messageHandlers(0))
  }

  override def subscribe(subject: String, queueGroup: String, messageHandlers: MessageHandler*): Subscription = {
    subscribe(subject, queueGroup, null, messageHandlers(0))

  }*/



  override def subscribe(s: String, messageHandlers: MessageHandler*): Subscription = {
    subscribe(s, null, 0, messageHandlers(0))
  }

  override def subscribe(s: String, s1: String, messageHandlers: MessageHandler*): Subscription = {
    subscribe(s, s1, 0, messageHandlers(0))
  }

  override def subscribe(subject: String, maxMessages: Integer, messageHandlers: MessageHandler*): Subscription = {
     subscribe(subject, null, maxMessages, messageHandlers(0))

  }

  override def subscribe(subject: String, queueGroup: String  , maxMessages: Integer , messageHandlers: MessageHandler*): Subscription = {
    assertNatsOpen()
    val id: String = Integer.toString(subscriptionId.incrementAndGet())
    val subscription: NatsSubscription = createSubscription(id, subject, queueGroup, maxMessages, messageHandlers(0))
    lock.synchronized{
      subscriptions.put(id, subscription)
      if(serverReady){
        writeSubscription(subscription)
      }
    }
    subscription
  }


  def createSubscription(id: String, subject: String, queueGroup: String, maxMessages: Integer, handlers: MessageHandler):NatsSubscription = {
    new NatsSubscription(subject, queueGroup, maxMessages, id, handlers){
      override def close(): Unit =lock.synchronized{
        super.close()
        subscriptions.remove(id)
        if(serverReady){
          channel.writeAndFlush(new ClientUnsubscribeFrame(id))
        }
      }

      override def createMessage(subject: String, body: String, queueGroup: String, replyTo: String): Message = {
        if(replyTo==null || replyTo.trim.length == 0){
          new DefaultMessage(subject, body, queueGroup, false)
        }
        new DefaultMessage(subject, body, queueGroup, true){
          @throws[UnsupportedOperationException]
          override def reply(body: String): Unit = {
            publish(replyTo, body)
          }

          @throws[UnsupportedOperationException]
          override def reply(body: String, delay: Long, timeUnit: TimeUnit): Unit = {
            eventLoopGroup.next.schedule(new Runnable() {
              override def run(): Unit = {
                publish(replyTo, body)
              }
            }, delay, timeUnit)
            super.reply(body, delay, timeUnit)
          }
        }
      }
    }
  }


  ////////////////
  ///// publish functions
  ////////////////
  override def publish(subject: String): Unit = {
    publish(subject,"",null)
  }

  override def publish(subject: String, body: String): Unit = {
    publish(subject, body, null)
  }

  override def publish(subject: String, body: String, replyTo: String): Unit ={
    assertNatsOpen()
    val publishFrame:ClientPublishFrame = new ClientPublishFrame(subject, body, replyTo)
    publish(publishFrame)
  }

  override def publish(subject: String, period: Long, timeUnit: TimeUnit): Registration = {
    publish(subject, "", null, period, timeUnit)
  }

  override def publish(subject: String, body: String, period: Long, timeUnit: TimeUnit): Registration = {
    publish(subject, body, null, period, timeUnit)
  }

  override def publish(subject: String, body: String, replyTo: String, period: Long, timeUnit: TimeUnit): Registration = {
    val publishFrame:ClientPublishFrame = new ClientPublishFrame(subject, body, replyTo)
    val scheduledFuture:ScheduledFuture[_] = eventLoopGroup.next()
      .scheduleAtFixedRate(new Runnable {
        override def run(): Unit = {
          if(isConnected()){
            publish(publishFrame)
          }
        }
      }, 0l, period, timeUnit)
    new Registration {
      override def remove(): Unit = {
        scheduledFuture.cancel(false)
      }
    }
  }

  def publish(publishFrame: ClientPublishFrame): Unit = lock synchronized {
    if(serverReady){
      channel.writeAndFlush(publishFrame)
    }else{
      publishQueue.add(publishFrame)
    }
  }

def assertNatsOpen(): Unit ={
  if(isClosed)throw new NatsClosedException()
}
  private def writeSubscription(subscription: NatsSubscription) = lock synchronized{
println(s"Channel= $channel")
    if (serverReady) channel.writeAndFlush(new ClientSubscribeFrame(subscription.getId, subscription.getSubject, subscription.getQueueGroup))

  }

   def fireStateChange(state: ConnectionStateListener.State) = {
    listeners.forEach((l: ConnectionStateListener) => {
      executor.execute(new Runnable() {
        override def run(): Unit = {
          l.onConnectionStateChange(MewClientNats.this, state)
        }
      })
    }
    )
  }
  //////////////////////
  /////// Inner classes
  //////////////////////

   class NatsSubscription protected(val subject: String, val queueGroup: String, val maxMessages: Integer, val id: String, val messageHandlers: MessageHandler) extends DefaultSubscription(subject, queueGroup, maxMessages, messageHandlers) {
     def getId = id
  }

   class NatsChannelInitializer(client: MewClientNats) extends ChannelInitializer[SocketChannel] {
    @throws[Exception]
    override def initChannel(channel: SocketChannel): Unit = {
      val pipeline = channel.pipeline
      pipeline.addLast("decoder", new ServerFrameDecoder(maxFrameSize))
      pipeline.addLast("encoder", new ClientFrameEncoder)
      pipeline.addLast("handler", new AbstractClientInboundMessageHandlerAdapter() {
        var idleCheckScheduledFuture:ScheduledFuture[_] = _
        var pingScheduledFuture:ScheduledFuture[_] = _

        override protected def publishedMessage(context: ChannelHandlerContext, frame: ServerPublishFrame): Unit = {
          resetIdleCheck(context.channel)
          val subscription = lock synchronized{ subscriptions.get(frame.getId)}

          if (subscription == null) {
            LOGGER.debug("Received a message with the subject '{}' with no subscribers.", frame.getSubject)
            return
          }
          subscription.onMessage(frame.getSubject, frame.getBody, frame.getReplyTo, executor)
        }

        override protected def pongResponse(context: ChannelHandlerContext, pongFrame: ServerPongFrame): Unit = {
          resetIdleCheck(context.channel)
        }

        override protected def serverInfo(context: ChannelHandlerContext, infoFrame: ServerInfoFrame): Unit = { // TODO Parse info body for alternative servers to connect to as soon as NATS' clustering support starts sending this.
          val server = serverList.getCurrentServer
          context.channel.writeAndFlush(new ClientConnectFrame(new ConnectBody(server.getUser, server.getPassword, pedantic, false))).addListener(new ChannelFutureListener() {
            @throws[Exception]
            override def operationComplete(future: ChannelFuture): Unit = {
              LOGGER.debug("Server ready")
              val channe = future.channel()
              lock synchronized{client.channel = channe }
              serverReady = true
              // Resubscribe when the channel opens.
              subscriptions.values().forEach((s:MewClientNats.this.NatsSubscription) => {
                writeSubscription(s)
              })

              // Resend pending publish commands.
              publishQueue.forEach((s:ClientPublishFrame) => {
                channel.write(s)
              })
              channel.flush

              pingScheduledFuture = channel.eventLoop.scheduleAtFixedRate(new Runnable() {
                override def run(): Unit = {
                  channel.writeAndFlush(ClientPingFrame.PING)
                }
              }, pingInterval, pingInterval, TimeUnit.MILLISECONDS)

              fireStateChange(ConnectionStateListener.State.SERVER_READY)
            }
          })
        }

        override

        protected def okResponse(context: ChannelHandlerContext, okFrame: ServerOkFrame): Unit = {
          // Ignore -- we're not using verbose so we won't get any
        }

        override

        protected def errorResponse(ctx: ChannelHandlerContext, errorFrame: ServerErrorFrame): Unit = {
          throw new NatsException("Sever error: " + errorFrame.getErrorMessage)
        }

        @throws[Exception]
        override def channelActive(context: ChannelHandlerContext): Unit = {
          fireStateChange(ConnectionStateListener.State.CONNECTED)
          resetIdleCheck(context.channel)
        }

        @throws[Exception]
        override def channelInactive(ctx: ChannelHandlerContext): Unit = lock synchronized{
           serverReady = false

          if (pingScheduledFuture != null) pingScheduledFuture.cancel(true)
          fireStateChange(ConnectionStateListener.State.DISCONNECTED)
          scheduleReconnect()
        }

        @throws[Exception]
        override def exceptionCaught(context: ChannelHandlerContext, cause: Throwable): Unit = {
          LOGGER.error("Error", cause)
        }

        private def resetIdleCheck(channel: Channel) = {
          if (idleCheckScheduledFuture != null) idleCheckScheduledFuture.cancel(true)
          idleCheckScheduledFuture = channel.eventLoop.schedule(new Runnable() {
            override def run(): Unit = {
              LOGGER.warn("Connection to NATS server has gone idle")
              channel.close
            }
          }, idleTimeout, TimeUnit.MILLISECONDS)
        }
      })
    }
  }

}




/*
 //(pprops : Properties, connHandler : MsgHandler, disconnHandler : MsgHandler)
 //extends Connection(pprops, connHandler, disconnHandler) {
 val subscriptionMap: ConcurrentHashMap[Integer, Subscription] = new ConcurrentHashMap[Integer, Subscription]

 val nc: NatsConnector = new NatsConnector().addHost("nats://localhost:4222")

 var nats: Nats = _ //nc.connect()


 def connect(): Unit = {
   val cf: CompletableFuture[Void] = new CompletableFuture()
   //netClient.connect(clientOptions.getPort, clientOptions.getHost, cf)
   // connecting = true
   nats = nc.connect()
   val c = isConnected(cf, nats)

 }
 // Closes this Nats instance
 def close(): Unit = {
   println("Closing connection...")
   nats.close()
 }

 //Checks if Nats is closed
 def isClosed():Unit = {
   nats.isClosed
 }

 //Checks if Nats is connecets
 def isConnected(cfConnect: CompletableFuture[Void], nats: Nats) = {
   println("gets here")
   val connected: Boolean = nats.isConnected()
   if (connected) {
     println("Connected")
     cfConnect.complete(null)
   } else {
     println("Not Connected")
     cfConnect.completeExceptionally(new Exception("Not Connected"))
   }
 }

 //Publishes a message with an empty body to the specified subject.
 def publish(subject: String): Unit ={
   println(s"Publish an empty message in channel '$subject'")
   nats.publish(subject)
 }

 //Publishes a message with an empty body to the specified subject on a recurring basis according to the specified period.
 // return a Registration
 def publish(subject: String, period: Long, unit: TimeUnit): Registration ={
   println(s"Publishes an empty message in channel $subject on a recurring basis with a period of $period.")
   nats.publish(subject, period, unit)
 }

 //Publishes a message with the provided body to the specified subject.
 def publish(subject: String, body: String )= {
 println(s"Publish '$body' in channel '$subject'")
   nats.publish(subject, body)
 }

 //Publishes a message with the provided body to the specified subject on a recurring basis according to the specified period.
 //returns a Registration
 def publish(subject: String, body: String, period: Long, unit: TimeUnit): Registration= {
   println(s"Publish '$body' in channel '$subject' on a recurring basis with period of $period")
   nats.publish(subject, body, period, unit)
 }

 //Publishes a message with the provided body to the specified subject.
 def publish(subject: String, body: String, reply: String)= {
   println(s"Publish '$body' in channel '$subject'")
   nats.publish(subject, body, reply)
 }

 //Publishes a message with the provided body to the specified subject.
 //returns a Registration
 def publish(subject: String, body: String, reply: String, period: Long, unit: TimeUnit): Registration= {
   println(s"Publish '$body' in channel '$subject'")
   nats.publish(subject, body, reply, period, unit)
 }

 //Subscribes to the specified subject and will automatically unsubscribe after the specified number of messages arrives.
 //returns a Subcription
 def subscribe(subject: String, maxMessages: Integer, messageHandler: MessageHandler ):Subscription={
   println(s"Subscribe to channel $subject and Unsibscribe after $maxMessages messages")
   val sub:Subscription = nats.subscribe(subject, maxMessages, messageHandler)
   subscriptionMap.
 }

 //Subscribes to the specified subject.
 //returns a Subcription
 def subscribe(subject: String, messageHandler: MessageHandler ):Subscription={
   println(s"Subscribe to channel $subject")
   nats.subscribe(subject, messageHandler)
 }

 //Subscribes to the specified subject within a specific queue group and will automatically unsubscribe after the specified number of messages arrives.
 //returns a Subcription
 def subscribe(subject: String, queueGroup: String, maxMessages: Integer, messageHandler: MessageHandler ):Subscription={
   println(s"Subscribe to channel $subject with queue group $queueGroup and Unsibscribe after $maxMessages messages")
   nats.subscribe(subject, queueGroup, maxMessages, messageHandler)
 }

 //Subscribes to the specified subject within a specific queue group.
 //returns a Subcription
 def subscribe(subject: String, queueGroup: String, messageHandler: MessageHandler ):Subscription={
   println(s"Subscribe to channel $subject with queue group $queueGroup")
   nats.subscribe(subject, queueGroup, messageHandler)
 }

 //Sends a request body on the given subject.
 //return a Request
 def request(subject: String, message: String, timeout: Long, unit: TimeUnit, maxReplies: Integer, messageHandler: MessageHandler): Request = {
   println(s"Send a request body on the channel $subject and waits for the maxReplies of $maxReplies")
   nats.request(subject, message, timeout, unit, maxReplies, messageHandler)
 }

 //Sends a request body on the given subject.
 //return a Request
 def request(subject: String, message: String, timeout: Long, unit: TimeUnit, messageHandler: MessageHandler): Request = {
   println(s"Send a request body on the channel $subject")
   nats.request(subject, message, timeout, unit, messageHandler)
 }

 //Callback function to use with subscribes and requests
 def onMessage(message: Message): Unit = {
   println("Message received:")
   println(message.getClass)
   println(message.getBody)
   println(message.getQueueGroup)
   println(message.getSubject)

 }



//Interface to listen for server connection state change events.
 override def onConnectionStateChange(nats: Nats, state: ConnectionStateListener.State): Unit = {
   println( nats.isConnected)
  val v =  state.name()
   println(v)
 }*/