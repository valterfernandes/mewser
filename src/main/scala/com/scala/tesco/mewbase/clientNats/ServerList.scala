package com.scala.tesco.mewbase.clientNats

import java.net.{InetSocketAddress, SocketAddress, URI}
import java.util
import java.util.Collections

import nats.Constants

/**
  * Created by Ricardo Martins on 30/08/2017.
  */
  class ServerList{
var RECONNECT_LIMIT: Int = 10

private val servers: util.ArrayList[Server] = new util.ArrayList[Server]

private var currentServer: Server = null

private var iterator: util.Iterator[Server] = null

private val lock: Object = new Object

def addServer (uri: URI): Unit = {
  var host: String = null
  var port: Int = 0
  if (uri.getHost == null) {
  host = Constants.DEFAULT_HOST
}
  else {
  host = uri.getHost
}
  if (uri.getPort > 0) {
  port = uri.getPort
}
  else {
  port = Constants.DEFAULT_PORT
}
  val address: SocketAddress = new InetSocketAddress (host, port)
  var user: String = null
  var password: String = null
  if (uri.getUserInfo != null) {
  val userInfo: String = uri.getUserInfo
  val parts: Array[String] = userInfo.split (":")
  if (parts.length >= 1) {
  user = parts (0)
  if (parts.length >= 2) {
  password = parts (1)
}
}
}
  lock.synchronized{servers.add(new Server(address, user, password) )
  }

}

  def addServers(servers: Iterable[URI] ): Unit = {
     for (uri <- servers) {
       addServer(uri)
     }
  }

  def nextServer:Server = lock synchronized {
    if (servers.size == 0) {
      throw new IllegalStateException("No servers in list.")
    }
    if (iterator == null || !(iterator.hasNext)) {
      val activeServers: util.ArrayList[Server] = new util.ArrayList[Server]

      servers.forEach((s) => {
        if (s.connectionAttempts > 0) activeServers.add(s)
      })

      /* for (server:Server <- servers) {
  if (server.connectionAttempts > 0) {
  activeServers.add (server)
}
}*/
      if (activeServers.size == 0) {

        servers.forEach(s => s.connectionAttempts = RECONNECT_LIMIT)
        /*for (server:Server <- servers) {

  server.connectionAttempts = RECONNECT_LIMIT
}*/
  activeServers.addAll(servers)
}
        Collections.shuffle(activeServers)
        iterator = activeServers.iterator
      }
      currentServer = iterator.next
      currentServer

    }


  def getCurrentServer: Server = lock synchronized{
    currentServer
}

  class Server(val address: SocketAddress, val user: String, val password: String) {
    var connectionAttempts = RECONNECT_LIMIT

    def getAddress: SocketAddress = address

    def getUser: String = user

    def getPassword: String = password

    def connectionFailure(): Unit =  lock synchronized{
      connectionAttempts -= 1

    }

    def connectionSuccess(): Unit = lock synchronized{
       connectionAttempts = RECONNECT_LIMIT

    }
  }

}