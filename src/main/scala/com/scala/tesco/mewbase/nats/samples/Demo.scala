package com.scala.tesco.mewbase.nats.samples

/**
  * Created by Tiago Filipe on 29/08/2017.
  */

import java.io.StringReader

import org.nats._
import java.util.Properties
import javax.json.Json
import javax.json.stream.JsonParser

import io.boson.json.{JsonExtractor, ObjectExtractor, StringExtractor}
import io.mewbase.bson.BsonObject

import scala.io.StdIn.readLine


object Demo {

  var parser: JsonParser = _

  val sentence: String = "{\"body\": \"vacation\", \"place\": {\"City\": \"LA\", \"Adress\": \"blablabla\" } }"
  val arB: Array[Byte] = sentence.getBytes

  def main(args: Array[String]){
    //Creating a BsonObject
    val temps: BsonObject = new BsonObject()
    temps.put("fridgeID", "f2").put("eventType", "Temperature").put("Temp_status", 5)

    println("Creating two clients: conn_1, conn_2")
    val conn_1 = Conn.connect(new Properties)
    val conn_2 = Conn.connect(new Properties)

    println("conn_1 subscribing")
    println("Listening on : " + args(0))

    val subS_1 : Integer = conn_1.subscribe(args(0), (msg:Msg) => {
      parser = Json.createParser(new StringReader(msg.body))
      val ext: JsonExtractor[String] = new ObjectExtractor[String](new StringExtractor("eventType"))
      val result: String = ext.apply(parser).getResult.toString
      println("conn_1: Received update : " + msg.body +
        " , Using Boson to extract field 'eventType': " + result)
    })

    /*val subB_1 : Integer = conn_1.subscribe(args(0) + "_bin", (msg:MsgB) => {println("conn_1 : Received update as (String)binary : " + new String(msg.body) +
      ". As binary: " + msg.body)})*/

    Thread.sleep(2000)

    println("conn_2 publishing")
    conn_2.publish(args(0), temps.toString.getBytes)
    //val json = new BsonObject(temps.toJsonObject) //transforms bson into json
  /*  conn_2.publish(args(0) + "_bin", "test".getBytes)

    Thread.sleep(2000)

    println("conn_2 subscribing subject 'help'")
    val sub_2 : Integer = conn_2.subscribe("help", (msg:Msg) => {conn_2.publish(msg.reply, "conn_2: I can help! 123")})
    println("conn_1 requesting help")
    conn_1.request("help", (msg:Msg) => {
      println("conn_1: Got a response for help: " + msg.body)
    })

    println("\nPress enter to exit.")
    readLine

    Thread.sleep(2000)

    println("Clients unsubscribing all subjects")
    conn_1.unsubscribe(subS_1)
    conn_1.unsubscribe(subB_1)
    conn_2.unsubscribe(sub_2)

    Thread.sleep(1000)

    println("Clients closed connections")
    conn_2.close()
    conn_1.close()

    Thread.sleep(1000)

    sys.exit*/
  }

}
