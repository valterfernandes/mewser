package com.scala.tesco.mewbase.nats.samples

/**
  * Created by Ricardo Martins on 28/08/2017.
  */

import java.util.Properties
import org.nats._
import scala.io.StdIn.readLine

object Sub {

  def main(args: Array[String]){
    var conn = Conn.connect(new Properties)

    println("Listening on : " + args(0))
    conn.subscribe(args(0), (msg:Msg) => {println("Received update : " + msg.body)})

    println("\nPress enter to exit.")
    readLine

    conn.close()
    sys.exit
  }
}