package com.scala.tesco.mewbase.nats.samples

/**
  * Created by Ricardo Martins on 28/08/2017.
  */
import java.util.Properties
import org.nats._

object ClusteredPub {

  def main(args: Array[String]){
    var opts : Properties = new Properties
    opts.put("servers", "nats://user1:pass1@localhost:4222,nats://user1:pass1@localhost:4248")
    var conn = Conn.connect(opts)

    println("Publishing...")
    conn.publish("hello", "world")

    conn.close()
    sys.exit
  }
}