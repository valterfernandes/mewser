package com.scala.tesco.mewbase.nats.samples

/**
  * Created by Ricardo Martins on 28/08/2017.
  */
import java.util.Properties
import org.nats._
import scala.io.StdIn.readLine
object SubTimeout {

  def main(args: Array[String]){
    var conn = Conn.connect(new Properties)

    println("Listening on : " + args(0))
    var received = 0
    var sid : Integer = conn.subscribe(args(0), (msg:Msg) => {received += 1})

    conn.timeout(sid, 1, null, (o:Object) => {println("Timeout waiting for a message!")})

    println("\nPress enter to exit.")
    readLine

    conn.close()
    sys.exit
  }
}