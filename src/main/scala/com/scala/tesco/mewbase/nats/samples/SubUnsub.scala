package com.scala.tesco.mewbase.nats.samples

/**
  * Created by Ricardo Martins on 28/08/2017.
  */

import java.util.Properties
import org.nats._
import scala.io.StdIn.readLine


object SubUnsub {

  def main(args: Array[String]){
    var conn = Conn.connect(new Properties)

    println("Subscribing...")
    var sid : Integer = conn.subscribe(args(0), (msg:Msg) => {println("Received update : " + msg.body)})

    println("\nPress enter to unsubscribe.")
    readLine

    conn.unsubscribe(sid)

    conn.close()
    sys.exit
  }
}