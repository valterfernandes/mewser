package com.scala.tesco.mewbase.common

import com.scala.tesco.mewbase.client.MewSubscription
import io.mewbase.bson.BsonObject
import io.mewbase.client.{ClientDelivery, Subscription}
import io.mewbase.common.Delivery

/**
  * Created by Ricardo Martins on 18/08/2017.
  */

//DeliveryImpl java class
class MewDelivery(val channels: String, val timestamps:Long, val channelsPos: Long, val events: BsonObject) extends Delivery {


  def channel(): String = channels

  def event(): BsonObject = events

  def timeStamp(): Long = timestamps

  def channelPos(): Long = channelsPos
}