package com.scala.tesco.mewbase.client

import java.lang
import java.util.concurrent.CompletableFuture

import io.mewbase.bson.BsonObject
import io.mewbase.client._
/**
  * Created by Ricardo Martins on 18/08/2017.
  */
class MewProducer(val client: MewClient, val channel:String, val id: Int) extends Producer {



  override def publish(bsonObject: BsonObject): CompletableFuture[Void] = client.doPublish(channel, id, bsonObject)

  override def close(): Unit = {
    client.removeProducer(id)}

  override def commitTx(): CompletableFuture[lang.Boolean] = null

  override def startTx(): Boolean = false

  override def abortTx(): CompletableFuture[lang.Boolean] = null
}
