package com.scala.tesco.mewbase.client

import java.{lang, util}
import java.util.concurrent.{CompletableFuture, ConcurrentHashMap, ConcurrentLinkedQueue}
import java.util.concurrent.atomic.AtomicInteger
import java.util.function

import io.mewbase.bson.{BsonArray, BsonObject}
import io.mewbase.client._
import io.mewbase.client.impl.ClientFrameHandler
import io.mewbase.common.SubDescriptor
import io.mewbase.server.impl.Protocol
import io.mewbase.util.AsyncResCF
import java.util.function.Consumer

import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.logging.LoggerFactory
import io.vertx.core.net.{NetClient, NetSocket}

import scala.util.Random

/**
  * Created by Ricardo Martins on 18/08/2017.
  */

object MewClient {
  val clientOptions = new ClientOptions()
  def main(args: Array[String]): Unit = {

    /**
      * Common code for Demos
      */

    println("Creating two Clients")
    val client1 =  new MewClient(clientOptions)
    //val client2 = new MewClient(clientOptions)

    //Creating an Event of type: CONNECT
    /*val frame: BsonObject = new BsonObject()
    frame.put(Protocol.CONNECT_VERSION, "0.1")
    val event: BsonObject = new BsonObject()
    event.put(Protocol.CONNECT_FRAME, frame)*/


    println("Both Clients establishing connection")
    client1.connect(new java.util.concurrent.CompletableFuture[BsonObject])
    //client2.connect(new java.util.concurrent.CompletableFuture[BsonObject])

    //Setting channel of the Subsdescriptor
    //val subD = new SubDescriptor
    //subD.setChannel("channel")

    this.synchronized{wait(2500)}

    /**
      * Demo of 2 clients subscribing a channel, publishing events,
      * unsubscribing and closing subscriptions.
      */
/*
    println("Client_1 Subscribes a channel")
    client1.subscribe(subD, (rsp: ClientDelivery) => {println(s"Client1 received and event: rsp=$rsp")
      val s:MewClientDelivery = rsp.asInstanceOf[MewClientDelivery]
      s.subscription().buffered.add(s)
      s.acknowledge()
    })

    println("Client_2 Subscribes same channel")
    client2.subscribe(subD,(rsp: ClientDelivery) => {println(s"Client2 received an event: rsp=$rsp")
      val s:MewClientDelivery = rsp.asInstanceOf[MewClientDelivery]
      s.subscription().buffered.add(s)
      s.acknowledge()
       })

    this.synchronized{wait(4000)}

    println("Client_1 Publishes an Event")
    client1.publish("channel", event)

    this.synchronized{wait(2500)}

    println("Client_1 does Unsubscribe")
    client1.doUnsubscribe(0)

    this.synchronized{wait(2500)}

    println("Client_2 publishes an Event")
    client2.publish("channel", event)

    this.synchronized{wait(1000)}

    println("Client_2 closes Subscription")
    client2.doSubClose(0)

    this.synchronized{wait(2500)}

    println("Both clients closing connection")
    client1.close()
    client2.close()
*/
    /**
      * Demo of client doing query to know the average of temperatures of a fridge
      *
      * Data generator is commented
      */
            /*
    //Generating random temperature and publishing it
    val temps: BsonObject = new BsonObject()

    val start = -5
    val end   = 16
    val rnd = new scala.util.Random
    temps.put("fridgeID", "f2").put("eventType", "Temperature")
    for(i <- 0 until 100){
      val tempVal = start + rnd.nextInt( (end - start) + 1 )
      val finalEv = temps.copy().put("Temp_status", tempVal)
      client1.publish("fridgeTemps",finalEv)
    }

    this.synchronized{wait(5000)}
            */

    //Querys server through findById
    val allTemps = client1.findByID("fridgesT", "f2").get()
    println("Sum of all fridge Temperatures: " + allTemps.getInteger("temp"))
    println("NumFridge Temperatures registered: " + allTemps.getInteger("numTemp"))
    println("Average Temperature(calculated client-side) = "+ allTemps.getInteger("temp")/allTemps.getInteger("numTemp"))
    println("Average Temperature(calculated server-side) = "+ allTemps.getInteger("average"))

    client1.close()
    /*this.synchronized{wait(5500)}

    println("Both clients closing connection")
    client1.close()
    client2.close()*/
  }
 //
}

class MewClient( val vertx: Vertx, val clientOptions: ClientOptions, val ownVertx: Boolean) extends Client with ClientFrameHandler {
  // Client Fields
  private val logger = LoggerFactory.getLogger(classOf[MewClient])

  private val sessionSeq: AtomicInteger = new AtomicInteger
  private val requestIDSequence: AtomicInteger = new AtomicInteger
  private val producerMap: ConcurrentHashMap[Integer, MewProducer] = new ConcurrentHashMap[Integer, MewProducer]
  val subscriptionMap: ConcurrentHashMap[Integer, MewSubscription] = new ConcurrentHashMap[Integer, MewSubscription]
  private val responseHandlers: ConcurrentHashMap[Integer, Consumer[BsonObject]] = new ConcurrentHashMap[Integer, Consumer[BsonObject]]
  private val queryResultHandlers: ConcurrentHashMap[Integer, Consumer[QueryResult]] = new ConcurrentHashMap[Integer, Consumer[QueryResult]]
  //var vertx: Vertx =
  val netClient: NetClient = vertx.createNetClient(clientOptions.getNetClientOptions)
  //var clientOptions: ClientOptions = _
  //var ownVertx: Boolean = _
  var netSocket: NetSocket = _
  var connecting: Boolean = _
  val bufferedWrites: util.Queue[Buffer] = new ConcurrentLinkedQueue[Buffer]
  var connectResponse: Consumer[BsonObject] = _

  //Client Construters
  //1st constructer
 /** def this(vertx: Vertx, clientOptions: ClientOptions, ownVertx: Boolean) {
    this()
    this.vertx = vertx
    this.netClient = vertx.createNetClient(clientOptions.getNetClientOptions)
    this.clientOptions = clientOptions
    this.ownVertx = ownVertx
  }*/
  //2nd Constructer
  def this( clientOptions: ClientOptions) {
    this( Vertx.vertx(), clientOptions, true)
  }
  //3rd Constructer
  def this( vertx: Vertx, clientOptions: ClientOptions) {
    this( vertx, clientOptions, false)
  }

  //Override Function
  override def createProducer(s: String): Producer = {
    val id: Int = sessionSeq.getAndIncrement()
    val prod: MewProducer = new MewProducer(this, s, id)
    producerMap.put(id, prod)
    prod
  }

  //Override Function
  override def subscribe(subDescriptor: SubDescriptor, consumer: function.Consumer[ClientDelivery]): CompletableFuture[Subscription] = {
    val cf: CompletableFuture[Subscription] = new CompletableFuture[Subscription]()
    var bsonObject: BsonObject = new BsonObject()
    if(subDescriptor.getChannel == null){
      throw new IllegalArgumentException("No Channel in SubDescriptor")
    }
    bsonObject.put(Protocol.SUBSCRIBE_CHANNEL, subDescriptor.getChannel)

    //@see wire_protocol.md
    if(subDescriptor.getStartEventNum != SubDescriptor.DEFAULT_START_NUM &&
    subDescriptor.getStartTimestamp != SubDescriptor.DEFAULT_START_TIME)
      throw new IllegalArgumentException("Cannot set both start position and start timestamp")

    bsonObject.put(Protocol.SUBSCRIBE_STARTPOS, subDescriptor.getStartEventNum)
    bsonObject.put(Protocol.SUBSCRIBE_STARTTIMESTAMP, subDescriptor.getStartTimestamp)

    bsonObject.put(Protocol.SUBSCRIBE_DURABLEID, subDescriptor.getDurableID)
    bsonObject.put(Protocol.SUBSCRIBE_FILTER_NAME, subDescriptor.getFilterName)
    write(cf, Protocol.SUBSCRIBE_FRAME, bsonObject, (resp:BsonObject) => {
      val ok:Boolean = resp.getBoolean(Protocol.RESPONSE_OK)
      if(ok){
        val subID = resp.getInteger(Protocol.SUBRESPONSE_SUBID)
        println("subID=" + subID)
        val sub: MewSubscription = new MewSubscription(subID, subDescriptor.getChannel, this, consumer.asInstanceOf[Consumer[MewClientDelivery]])
        subscriptionMap.put(subID, sub)
        cf.complete(sub)
      }else{
        println("Error subID")
        cf.completeExceptionally(responseToException(resp))
      }
    })
    cf
  }

  //Override Function
  override def publish(channel: String, bsonObject: BsonObject): CompletableFuture[Void] = doPublish(channel, -1, bsonObject)

  //Override Function
  override def publish(channel: String, bsonObject: BsonObject, func: function.Function[BsonObject, String]): CompletableFuture[Void] = {
    //TODO func
    doPublish(channel, -1, bsonObject)
  }

  //Override Function
  override def findByID(binderName: String, id: String): CompletableFuture[BsonObject] = {
    val cf: CompletableFuture[BsonObject] = new CompletableFuture[BsonObject]()
    val bsonObject:BsonObject = new BsonObject()
    bsonObject.put(Protocol.FINDBYID_BINDER, binderName)
    bsonObject.put(Protocol.FINDBYID_DOCID, id)
    write(cf, Protocol.FINDBYID_FRAME, bsonObject, resp => {
      val ok: Boolean = resp.getBoolean(Protocol.RESPONSE_OK)
      if(ok){
        val result = resp.getBsonObject(Protocol.FINDRESPONSE_RESULT)
        cf.complete(result)
      } else {
        cf.completeExceptionally(responseToException(resp))
      }
    })
    cf
  }

  //Override Function
  def writeQuery(bsonObject: BsonObject, resultHandler: Consumer[QueryResult], cf: CompletableFuture[_]): Unit ={
    val queryID: Int = requestIDSequence.getAndIncrement()
    bsonObject.put(Protocol.QUERY_QUERYID, queryID)
    queryResultHandlers.put(queryID, resultHandler)
    write(cf, Protocol.QUERY_FRAME, bsonObject)
  }

  /*//Override Function
  override def findMatching(binderName: String, matcher: BsonObject, resultHandler: function.Consumer[QueryResult], exceptionHandler: function.Consumer[Throwable]): Unit = {
    val cf: CompletableFuture[Unit] = new CompletableFuture[Unit]()
    if(exceptionHandler != null){
      cf.exceptionally(t => {
        exceptionHandler.accept(t)
        //null
      })
    }
    val bsonObject: BsonObject = new BsonObject()
    bsonObject.put(Protocol.QUERY_BINDER, binderName)
    bsonObject.put(Protocol.QUERY_MATCHER, matcher)
    writeQuery(bsonObject, resultHandler, cf)
  }*/

  //Override Function
  override def close(): CompletableFuture[Void] = {
    netClient.close()
    if (ownVertx) {
      val cf: AsyncResCF[Void] = new AsyncResCF[Void]
      vertx.close()
      cf
    }else{
      CompletableFuture.completedFuture(null)
    }
  }


  // Frame Handler

  //Override Function
  override def handleQueryResult(size: Int, bsonObject: BsonObject): Unit = {
    val rQueryID: Int = bsonObject.getInteger(Protocol.QUERYRESULT_QUERYID)
    val qrh: Consumer[QueryResult] = queryResultHandlers.get(rQueryID)
    if(qrh == null){
      throw new IllegalArgumentException("Cant find query result handler")
    }
    val last: Boolean = bsonObject.getBoolean(Protocol.QUERYRESULT_LAST)
    val qr:QueryResult = new MewQueryResult(bsonObject.getBsonObject(Protocol.QUERYRESULT_RESULT), size, last, rQueryID)
    try{
      qrh.accept(qr)
    }finally {
      if(last){
        queryResultHandlers.remove(rQueryID)
      }
    }
  }

  //Override Function
  override def handleRecev(i: Int, bsonObject: BsonObject): Unit = {
    val subID:Int = bsonObject.getInteger(Protocol.RECEV_SUBID)
  //  println(s"handleRecev:  i= $i event= $bsonObject")
    val sub: MewSubscription = subscriptionMap.get(subID)

   // println(s"MewSubscription sub= $sub")
    if(sub == null){
      // No subscription for this - maybe closed - ignore
    }else{
      sub.handleRecevFrame(i, bsonObject)
    }

  }

  //Override Function
  override def handlePing(bsonObject: BsonObject): Unit = {}

  //Override Function
  override def handleSubResponse(bsonObject: BsonObject): Unit = {
    handleResponse(bsonObject)
  }

  //Override Function
  override def handleResponse(bsonObject: BsonObject): Unit ={
    println(s"Response from Server: Frame: $bsonObject ${bsonObject.fieldNames()}")
    if (connecting) connectResponse.accept(bsonObject)
    else {
      val requestID = bsonObject.getInteger(Protocol.RESPONSE_REQUEST_ID)
      if (requestID == null) throw new IllegalStateException("No request id in response: " + bsonObject)
      val respHandler = responseHandlers.remove(requestID)
      if (respHandler == null) throw new IllegalStateException("Unexpected response")
      respHandler.accept(bsonObject)
    }
  }
  //Override Function
  protected def write(cf: CompletableFuture[_], frameType: String, frame: BsonObject): Unit = this.synchronized {
    write(cf, frameType, frame, (fr: BsonObject) => {

    })
  }

  //Override Function
  protected def write(cf: CompletableFuture[_], frameType: String, frame: BsonObject, respHandler: Consumer[BsonObject]): Unit = this.synchronized {
    if (respHandler != null) {
      val requestID = requestIDSequence.getAndIncrement
      frame.put(Protocol.RESPONSE_REQUEST_ID, requestID)
    //  println(s"(write)rID=$requestID")
    //  println(s"(write) event == null : ${frame.getBsonObject(Protocol.PUBLISH_EVENT)==null}" )
    //  println(s"(write) channel = ${frame.getString(Protocol.PUBLISH_CHANNEL)}" )
      responseHandlers.put(requestID, respHandler)
    }
    val buff = Protocol.encodeFrame(frameType, frame)
    if (connecting || netSocket == null) {
      if (!connecting) connect(cf)
      bufferedWrites.add(buff)
    }
    else netSocket.write(buff)
  }

  //Override Function
  protected def write(buff: Buffer): Unit = {
    if (netSocket == null) throw new MewException("Not connected")
    netSocket.write(buff)
  }

  //Override Function
  private def connect(cfConnect: CompletableFuture[_]) = {
    val cf = new AsyncResCF[NetSocket]
    netClient.connect(clientOptions.getPort, clientOptions.getHost, cf)
    connecting = true
    cf.thenAccept((ns: NetSocket) => sendConnect(cfConnect, ns)).exceptionally((t: Throwable) => {
      def foo(t: Throwable) = {
        cfConnect.completeExceptionally(t)
        null
      }

      foo(t)
    })
  }

  //Override Function
  def doUnsubscribe(subID: Int): Unit = {
    subscriptionMap.remove(subID)
    val frame = new BsonObject
    frame.put(Protocol.UNSUBSCRIBE_SUBID, subID)
    val cf = new CompletableFuture[BsonObject]
    write(cf, Protocol.UNSUBSCRIBE_FRAME, frame, resp =>
    {
      val ok: Boolean = resp.getBoolean(Protocol.RESPONSE_OK)
      if(ok){
        println(s"Unsubscribe $resp")
        cf.complete(null)
      }else{
        cf.completeExceptionally(responseToException(resp))
      }
    })
    //cf.thenAccept(p => println(p.fieldNames().toString))
  }

  //Override Function
  def doSubClose(subID: Int): Unit = {
    subscriptionMap.remove(subID)
    val frame = new BsonObject
    frame.put(Protocol.SUBCLOSE_SUBID, subID)
    val cf = new CompletableFuture[BsonObject]
    write(cf, Protocol.SUBCLOSE_FRAME, frame)
  }

  //Override Function
  def doAckEv(subID: Int, pos: Long, sizeBytes: Int): Unit = {
    val frame = new BsonObject
    frame.put(Protocol.ACKEV_SUBID, subID)
    frame.put(Protocol.ACKEV_POS, pos)
    frame.put(Protocol.ACKEV_BYTES, sizeBytes)
    val buffer = Protocol.encodeFrame(Protocol.ACKEV_FRAME, frame)
    write(buffer)
  }

  //Override Function
  protected def doQueryAck(queryID: Int, bytes: Int): Unit ={
    val bsonObject: BsonObject = new BsonObject()
    bsonObject.put(Protocol.QUERYACK_QUERYID, queryID)
    bsonObject.put(Protocol.QUERYACK_BYTES, bytes)
    val buffer: Buffer = Protocol.encodeFrame(Protocol.QUERYACK_FRAME, bsonObject)
    write(buffer)
  }


  //Override Function
 def doPublish(channel: String, producerID: Int, event: BsonObject): CompletableFuture[Void] = {
    val cf = new CompletableFuture[Void]
    val frame = new BsonObject
    frame.put(Protocol.PUBLISH_CHANNEL, channel)
    frame.put(Protocol.PUBLISH_SESSID, producerID)
    frame.put(Protocol.PUBLISH_EVENT, event)
    write(cf, Protocol.PUBLISH_FRAME, frame, (resp: BsonObject) => {
      def foo(resp: BsonObject) = {
        val ok = resp.getBoolean(Protocol.RESPONSE_OK)
        if(ok) println("Pusblished")
        else println("Error:Publish")

        if (ok) cf.complete(null)
        else cf.completeExceptionally(responseToException(resp))
      }

      foo(resp)
    })
    cf
  }

  //Removes a Producer
  def removeProducer(id: Int):Unit= {
    producerMap.remove(id)
  }

  //Responde to an exception
  private def responseToException(resp: BsonObject) = new MewException(resp.getString(Protocol.RESPONSE_ERRMSG), resp.getInteger(Protocol.RESPONSE_ERRCODE))

  //Sends a Connection request
  private def sendConnect(cfConnect: CompletableFuture[_], ns: NetSocket) = this.synchronized {
    println("Sending Connect")
    netSocket = ns
    netSocket.handler(new Protocol(this).recordParser)
    val authInfo = clientOptions.getAuthInfo
    // Send the CONNECT frame
    val frame = new BsonObject
    frame.put(Protocol.CONNECT_VERSION, "0.1")
    frame.put(Protocol.CONNECT_AUTH_INFO, authInfo)
    val buffer = Protocol.encodeFrame(Protocol.CONNECT_FRAME, frame)
    connectResponse = (resp: BsonObject) => connected(cfConnect, resp)
    netSocket.write(buffer)
  }

  //Verifies if the connection was successful
  private def connected(cfConnect: CompletableFuture[_], resp: BsonObject) = this.synchronized {
    connecting = false
    var flag:Boolean = true
    val ok = resp.getBoolean(Protocol.RESPONSE_OK)

    if (ok){println("Connected")}else{println("Not Connected")}
    if (ok) while (flag) {
      val buff = bufferedWrites.poll
      if (buff == null)flag = false //break //todo: break is not supported
      else netSocket.write(buff)
    }
    else cfConnect.completeExceptionally(new MewException(resp.getString(Protocol.RESPONSE_ERRMSG), resp.getInteger(Protocol.RESPONSE_ERRCODE)))
  }

  // MewQueryResult inner class
   class MewQueryResult extends QueryResult{
    // class fields
    var documents: BsonObject = _
    var bytes: Int = _
    var last: Boolean = _
    var queryID: Int = _

    def this(doc: BsonObject, bytes: Int, last: Boolean, queryID: Int){
      this()
      this.documents = doc
      this.bytes=bytes
      this.last=last
      this.queryID = queryID
    }

    override def isLast: Boolean = last

    override def document(): BsonObject = documents

    override def acknowledge(): Unit = doQueryAck(queryID, bytes)
  }


  /////
  // The run method to start the client
  def run(): Unit ={
    val bs: BsonObject = new BsonObject()

    this.publish(Protocol.CONNECT_FRAME, bs)

  }

  override def createChannel(s: String): CompletableFuture[lang.Boolean] = ???

  override def createBinder(s: String): CompletableFuture[lang.Boolean] = ???

  override def listBinders(): CompletableFuture[BsonArray] = ???

  override def executeQuery(s: String, bsonObject: BsonObject, resultHandler: Consumer[QueryResult],
                            exceptionHandler: Consumer[Throwable]): Unit = ??? /*{
    val cf = new CompletableFuture[Void]()
    if(exceptionHandler != null){
      cf.exceptionally(t => {
        exceptionHandler.accept(t)
        null
      })
    }
    val
  }*/

  override def sendCommand(s: String, bsonObject: BsonObject): CompletableFuture[Void] = ???

  override def listChannels(): CompletableFuture[BsonArray] = ???
}