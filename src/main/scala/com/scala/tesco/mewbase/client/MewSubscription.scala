package com.scala.tesco.mewbase.client

import java.util
import java.util.function.Consumer

import io.mewbase.bson.BsonObject
import io.mewbase.client.impl.ClientDeliveryImpl
import io.mewbase.client.{ClientDelivery, Subscription}
import io.mewbase.server.impl.Protocol
import io.vertx.core.{Context, Vertx}

/**
  * Created by Ricardo Martins on 18/08/2017.
  */
class MewSubscription(val id: Int, val channel: String, val mewClient: MewClient, val consumer: Consumer[MewClientDelivery]) extends Subscription{

  val ctx: Context = Vertx.currentContext()
  val buffered:util.Queue[MewClientDelivery] = new util.LinkedList[MewClientDelivery]()
  var closed: Boolean = _
  var paused: Boolean = _

  /////override methods
  override def resume(): Unit = this synchronized {
    var flag:Boolean=true
    if (paused) {
      paused = false
      while ( {
        !paused && flag
      }) {
        val del = buffered.poll
        if (del == null) flag = false //todo: break is not supported
        consumer.accept(del)
      }
    }
  }

  override def receive(l: Long): BsonObject = ???
  override def pause(): Unit = paused = true

  override def unsubscribe(): Unit = {
    mewClient.doUnsubscribe(id)
    this synchronized {closed = true}

  }

  override def close(): Unit = {
    mewClient.doSubClose(id) // Outside sync block to prevent deadlock

   this synchronized {closed = true}

  }

  //other methods
   def handleRecevFrame(size: Int, frame: BsonObject): Unit = {
    if (closed) return
    checkContext()
    // println(s"MewSubscription + $frame ")
     //println(s"Bufferered0: $buffered ")
     val delivery = new MewClientDelivery(channel, frame.getLong(Protocol.RECEV_TIMESTAMP), frame.getLong(Protocol.RECEV_POS), frame.getBsonObject(Protocol.RECEV_EVENT), this, size)
   if (!paused) consumer.accept(delivery)
    else buffered.add(delivery)

     println(s"Stored deliveries (Bufferered): $buffered ")
  }

  // Sanity check - this should always be executed using the connection's context
  private def checkContext() = {
    if (Vertx.currentContext ne ctx) throw new IllegalStateException("Wrong context!")
  }

   def acknowledge(pos: Long, sizeBytes: Int): Unit = {
    mewClient.doAckEv(id, pos, sizeBytes)
  }
}
