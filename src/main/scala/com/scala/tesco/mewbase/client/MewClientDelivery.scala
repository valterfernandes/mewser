package com.scala.tesco.mewbase.client

import com.scala.tesco.mewbase.common.MewDelivery
import io.mewbase.bson.BsonObject
import io.mewbase.client.ClientDelivery


/**
  * Created by Ricardo Martins on 18/08/2017.
  */
class MewClientDelivery(channels: String,timestamps: Long,
                        channelsPos: Long, events: BsonObject, sub: MewSubscription, sizeBytes: Int) extends MewDelivery(channels:String, timestamps:Long, channelsPos: Long, events: BsonObject)  with ClientDelivery {


  override def subscription(): MewSubscription = sub

  override def acknowledge(): Unit = {
    sub.acknowledge(channelsPos, sizeBytes)
  }
}
