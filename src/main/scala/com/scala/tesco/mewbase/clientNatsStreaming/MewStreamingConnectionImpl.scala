package com.scala.tesco.mewbase.clientNatsStreaming


import io.nats.streaming.protobuf.StartPosition._
import java.io.{IOException, InputStream, StringReader}
import java.lang.Class
import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.time.{Duration, Instant}
import java.time.temporal.ChronoUnit
import java.util
import java.util.{Date, Timer, TimerTask}
import java.util.concurrent._
import java.util.concurrent.locks.{ReadWriteLock, ReentrantReadWriteLock}
import javax.json.Json
import javax.json.stream.JsonParser

import com.fasterxml.jackson.core.`type`.TypeReference
import com.fasterxml.jackson.core.io.IOContext
import com.scala.tesco.mewbase
//import com.scala.tesco.mewbase.bsonPath.TestTinyLanguage.{finalExpr, parse}
import com.scala.tesco.mewbase.bsonPath.TinyLanguage
import com.scala.tesco.mewbase.clientNatsStreaming
import com.scala.tesco.mewbase.nettybson.Constants.charset
import io.mewbase.bson.BsonArray
import io.vertx.core.buffer.Buffer
import com.scala.tesco.mewbase.nettybson.Constants._
import io.mewbase.eventsource.{EventSink, Subscription}
import io.mewbase.eventsource.impl.nats.{NatsEventSink, NatsEventSource}

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.util.Try
//import com.scala.tesco.mewbase.nettybson.{NettyBson}
import com.scala.tesco.mewbase.nettybson._
import io.boson.json.{JsonExtractor, NumberExtractor, ObjectExtractor, StringExtractor}
import io.boson.valid.Validation
import io.mewbase.bson.{Bson, BsonObject}
import io.nats.client.{Connection, NUID, Nats, Options}
import io.nats.streaming.protobuf._
import shaded.nats.com.google.protobuf.{ByteString, InvalidProtocolBufferException}
import io.netty.buffer
import io.netty.buffer._
import io.vertx.core.json.JsonObject


/**
  * Created by Ricardo Martins on 31/08/2017.
  */
//nats-streaming-server -m 8222 -store file -dir datastore
//docker run -p 4222:4222 -p 6222:6222 -p 8222:8222 --name NatsS nats-streaming -store file -dir datastore
//LINK STREAM TO NATS => docker run -ti --name=natsS --link nats nats-streaming -store file -dir datastore -ns nats://nats:9222
//CREATE CLUSTER => docker run -it -p 0.0.0.0:8222:8222 -p 0.0.0.0:7246:7246 --rm -v "C:/Users/Tiago Filipe/Desktop/Go"/confB.conf:/tmp/cluster.conf apcera/gnatsd -c /tmp/cluster.conf -p 8222 -D -V
object MewStreamingConnectionImpl {

  val nettyBufPublish: NettyBson = new NettyBson()
  val charset: Charset = java.nio.charset.Charset.availableCharsets().get("UTF-8")


  def main(args: Array[String]): Unit = {}

   /* //Create Client Factory and generate a client
    val cf_1: MewStreamingConnectionFactory = new MewStreamingConnectionFactory("test-cluster", "Client_T1", "nats://localhost:4222") //Choosing which server to connect
    val clientT1: MewStreamingConnection = cf_1.createConnection()
  clientT1.getNatsConnection
    //Subscribe to subject channel
    clientT1.subscribe("channel", msg => {
      println("message received")
      //Create a Netty Buffer to handle the received information
      val arrayOption: Option[Array[Byte]] = Option(msg.getData.get)
      val nettyBufSubscribe: NettyBson = new NettyBson(arrayOption)

      try {
        val bool: Any = nettyBufSubscribe.extractField("kitchen")
        println("Bytes " + new String(bool.asInstanceOf[Array[Byte]]))
      } catch {
        case e: NoSuchFieldException => println(s"${e.getMessage}")
      }
      try {
        val bool1: Any = nettyBufSubscribe.extractField("nTables")
        println("Int " + bool1)
      } catch {
        case e: NoSuchFieldException => println(s"${e.getMessage}")
      }
      try {
        val bool2: Any = nettyBufSubscribe.extractField("DoorOpen")
        println("Bool " + bool2)
      } catch {
        case e: NoSuchFieldException => println(s"${e.getMessage}")
      }
      try {
        val bool3: Any = nettyBufSubscribe.extractField("Grade")
        println("Char " + bool3)
      } catch {
        case e: NoSuchFieldException => println(s"${e.getMessage}")
      }
      try {
        val bool4: Any = nettyBufSubscribe.extractField("CharSequence")
        println("CharSequence " + bool4)
      } catch {
        case e: NoSuchFieldException => println(s"${e.getMessage}")
      }
    })

    //Create BsonObject with thw information to send    put("fridgeID", "f2").
    val temperatures: BsonObject = new BsonObject()
    val double: Double = 10
    val short: Short = 1
    temperatures.put("fridgeID", "f2").put("eventType", false).put("something", true).put("Double", double).put("Short", short)

    //Final netty buffer after convert the BsonObject to Netty Buffer
    val finalBuf: NettyBson = nettyBufPublish.Bson2ByteBuf(temperatures)

    val nettyBson: NettyBson = new NettyBson()
    val finalNettyBuf: NettyBson = nettyBson.writeBytes("kitchen", "dirty".getBytes).writeInt("nTables", 1)
      .writeBoolean("DoorOpen", false).writeChar("Grade", 'C').writeCharSequence("CharSequence", "It WORKS!!!", charset)
    //val newNettyBson: NettyBson = nettyBson.writeCharSequence("FirstField", "ITS A ME", charset).writeCharSequence("Name", "Mario", charset).writeBytes("Bytes", "field of bytes".getBytes)

    //Publish to the Server
    clientT1.publish("channel", finalNettyBuf.array, (msg, err) => {
      println("Ack received")
    })*/

  /*val obj1: BsonObject = new BsonObject().put("André", 2.2f).put("António", 1.5)
  val obj2: BsonObject = new BsonObject().put("André", 1250L).put("José", false)
  val obj3: BsonObject = new BsonObject().put("André", 1500).putNull("Amadeu")

  val arr: BsonArray = new BsonArray().add(obj1).add(obj2).add(obj3)

  val bsonEvent: BsonObject = new BsonObject().put("StartUp", arr)

  val nettyBson: NettyBson = new NettyBson(vertxBuff = Option(bsonEvent.encode()))

  //val eSink: EventSink = new NatsEventSink()
  //val eSource = new NatsEventSource()

  val testChannelName: String = "singleEventSink"

  //eSource.subscribe(testChannelName, event => {
    //val rcvNettyBson: NettyBson = new NettyBson(vertxBuff = Option(bsonEvent.encode())) //this was it works
    val rcvNettyBson: NettyBson = new NettyBson(byteBuf = Option(nettyBson.getByteBuf)) //this way ain't working
    val key: String = "André"
    val result = rcvNettyBson.extractFromNetty(rcvNettyBson.getByteBuf, key).get
    println(s"Result from extracting $key -> $result")
  //})
*/
  val br4: BsonArray = new BsonArray().add("Insecticida")
  val br1: BsonArray = new BsonArray().add("Tarantula").add("Aracnídius").add(br4)
  val obj1: BsonObject = new BsonObject().put("José", br1)

  val br2: BsonArray = new BsonArray().add("Spider")
  val obj2: BsonObject = new BsonObject().put("José", br2)//.put("Alves", false)

  val br3: BsonArray = new BsonArray().add("Fly")
  val obj3: BsonObject = new BsonObject().put("José", br3)//.put("Alves", "we dem boys".getBytes)


  //val arr2: BsonArray = new BsonArray().add("Jesus").add(obj3)
  //val obj4: BsonObject = new BsonObject().put("José", arr2)

  val arr: BsonArray = new BsonArray().add(obj1).add(obj2).add(obj3).add(br4)
  val bsonEvent: BsonObject = new BsonObject().put("StartUp", arr)//.put("Test", arr2)
  //val nettyBson: NettyBson = new NettyBson(vertxBuff = Option(bsonEvent.encode()))


  val key: String = ""
  //val rcvNettyBson: NettyBson = new NettyBson(byteBuf = Option(nettyBson.getByteBuf))
  //val parser = new TinyLanguage
  val language: String = "first 0 to end]"

  val arrTest: BsonArray = new BsonArray().add(2.2).add(2.4).add(2.6).add(arr)

  val netty: NettyBson = new NettyBson(vertxBuff = Option(arrTest.encode()))
  //val resultParser: Try[Any] = Try(parse(finalExpr(netty, key), language).get)
  //println(s"resultParser $resultParser")
  //println("TESTING SOME SHIT: " + netty.extract(new NettyBson(byteBuf = Option(netty.getByteBuf.duplicate())).getByteBuf, key, language).get)

 /*try {
    val resultParser: Any = parse(expr(new NettyBson(byteBuf = Option(nettyBson.getByteBuf.duplicate())), key), language).get
    println()
    println(s"Result of extraction -> $resultParser , with TinyLanguage -> $language")

  } catch {
    case e: IllegalArgumentException => println(e.getMessage)
  }

  val language1: String = "stddev"
  try {
    val resultParser: Any = parse(expr(new NettyBson(byteBuf = Option(nettyBson.getByteBuf.duplicate())), key), language1).get
    println()
    println(s"Result of extraction -> $resultParser , with TinyLanguage -> $language1")

  } catch {
    case e: IllegalArgumentException => println(e.getMessage)
  }*/

  //eSink.publish(testChannelName, nettyBson.array)
}


class MewStreamingConnectionImpl(val stanClusterId: String = null, val clientId: String = null, val opts: Option[MewOptions] = Option(null))
  extends MewStreamingConnection with io.nats.client.MessageHandler {


  val mu: ReadWriteLock = new ReentrantReadWriteLock()

  //var clusterId : String = _
  var pubPrefix: String = _ // Publish prefix set by streaming, append our subject.
  var subRequests: String = _ // Subject to send subscription requests.
  var unsubRequests: String = _ // Subject to send unsubscribe requests.
  var subCloseRequests: String = _
  // Subject to send subscription close requests.
  var closeRequests: String = _
  // Subject to send close requests.
  var ackSubject: String = _
  // publish acks
  var ackSubscription: io.nats.client.Subscription = _
  var hbSubscription: io.nats.client.Subscription = _
  var hbCallback: io.nats.client.MessageHandler = _

  var subMap: util.HashMap[String, MewSubscriptionImpl] = _
  var pubAckMap: util.HashMap[String, AckClosure] = _
  var pubAckChan: BlockingQueue[PubAck] = _
  var nc: Connection = _

  val ackTimer: Timer = new Timer(true)

  var ncOwned = false

  val exec: ExecutorService = Executors.newCachedThreadPool()

  val newOpts: MewOptions =
    opts match {
      case None => new clientNatsStreaming.MewOptions.Builder().build()
      case Some(op) if op.getNatsConn != null =>
        setNatsConnection(op.getNatsConn)
        op
      case Some(op) => op
    }

  // Connect will form a connection to the STAN subsystem.
  @throws[IOException]
  @throws[InterruptedException]
  def connect(): MewStreamingConnectionImpl = {
    var exThrown: Boolean = false
    val nc1: Option[Connection] = Option(getNatsConnection())
    // Create a NATS connection if it doens't exist
    val nc2: Connection = nc1 match {
      case None =>
        val nc2: Connection = createNatsConnection
        setNatsConnection(nc2)
        ncOwned = true
        nc2
      case Some(connection) =>
        if (!connection.isConnected) throw new IOException(MewNatsStreaming.ERR_BAD_CONNECTION)
        connection
    }

    try {
      // Create a heartbeat inbox
      val hbInbox: String = nc2.newInbox
      hbCallback = msg => processHeartBeat(msg)
      hbSubscription = nc2.subscribe(hbInbox, hbCallback)

      // Send Request to discover the cluster
      val discoverSubject: String = s"${newOpts.getDiscoverPrefix}.$stanClusterId"
      val req: ConnectRequest = ConnectRequest.newBuilder().setClientID(clientId).setHeartbeatInbox(hbInbox).build()
      val bytes: Array[Byte] = req.toByteArray
      val reply: io.nats.client.Message = nc2.request(discoverSubject, bytes, newOpts.getConnectTimeout.toMillis)
      if (reply == null) {
        throw new IOException(MewNatsStreaming.ERR_CONNECTION_REQ_TIMEOUT)
      }
      val cr: ConnectResponse = ConnectResponse.parseFrom(reply.getData)
      if (!cr.getError.isEmpty) throw new IOException(cr.getError)

      // Capture cluster configuration endpoints to publish and
      // subscribe/unsubscribe.
      pubPrefix = cr.getPubPrefix
      subRequests = cr.getSubRequests
      unsubRequests = cr.getUnsubRequests
      subCloseRequests = cr.getSubCloseRequests
      closeRequests = cr.getCloseRequests

      // Setup the ACK subscription
      ackSubject = String.format(s"${MewNatsStreaming.DEFAULT_ACK_PREFIX}.${NUID.nextGlobal()}")
      ackSubscription = nc2.subscribe(ackSubject, msg => processAck(msg))
      ackSubscription.setPendingLimits(1024 ^ 2, 32 * 1024 ^ 2)
      pubAckMap = new util.HashMap[String, AckClosure]() //String, Option[AckClosure]

      // Create Subscription map
      subMap = new util.HashMap[String, MewSubscriptionImpl]() //[String, MewSubscription]

      pubAckChan = new LinkedBlockingQueue(newOpts.getMaxPubAckInFlight) //[PubAck]
    } catch {
      case e: IOException =>
        exThrown = true
        if (io.nats.client.Nats.ERR_TIMEOUT.equals(e.getMessage)) {
          throw new IOException(MewNatsStreaming.ERR_CONNECTION_REQ_TIMEOUT, e)
        } else {
          throw e
        }
    } finally {
      if (exThrown) {
        try {
          close()
        } catch {
          case _: Exception => /* NOOP -- can't do anything if close fails */
        }
      }
    }
    this
  }

  @throws[IOException]
  @throws[InterruptedException]
  override def subscribe(subject: String, cb: MewMessageHandler): MewSubscription = {
    subscribe(subject, cb, null)
  }

  @throws[IOException]
  @throws[InterruptedException]
  override def subscribe(subject: String, cb: MewMessageHandler, opts: MewSubscriptionOptions): MewSubscription = {
    subscribe(subject, null, cb, opts)
  }

  @throws[IOException]
  @throws[InterruptedException]
  override def subscribe(subject: String, queue: String, cb: MewMessageHandler): MewSubscription = subscribe(subject, queue, cb, null)


  @throws[IOException]
  @throws[InterruptedException]
  override def subscribe(subject: String, queue: String, cb: MewMessageHandler, opts: MewSubscriptionOptions): MewSubscription = {
    val q: Option[String] =
      Option(queue)
    val cbH: Option[MewMessageHandler] =
      Option(cb)
    val opt: Option[MewSubscriptionOptions] =
      Option(opts)
    subscribe(subject, q, cbH, opt)
  }

  def subscribe(str: String, q: Option[String], cbH: Option[MewMessageHandler], opt: Option[MewSubscriptionOptions]): MewSubscription = {
    val subject: String = str
    val queue: String = q match {
      case None => null
      case Some(o) => o
    }
    val cb: MewMessageHandler = cbH match {
      case None => null
      case Some(o) => o
    }

    val opts: MewSubscriptionOptions = opt match {
      case None => null
      case Some(o) => o
    }

    this.lock()
    val (nc: io.nats.client.Connection, sub: MewSubscriptionImpl) =
      try {
        if (getNatsConnection == null) {
          throw new IllegalStateException(MewNatsStreaming.ERR_CONNECTION_CLOSED)
        }
        val sub1: MewSubscriptionImpl = createSubscription(subject, queue, cb, this, opts)

        subMap.put(sub1.getInbox, sub1)
        // Register subscription
        val nc1: io.nats.client.Connection = getNatsConnection()

        (nc1, sub1)
      } finally {
        this.unlock()
      }

    //Hold lock throughout
    sub.wLock()
    try {


      sub.inboxSub = Option(nc.subscribe(sub.getInbox, this))
      //Listen for actual messages
      if (sub == null) println("sub is null")
      val sr: SubscriptionRequest = createSubscriptionRequest(sub)
      val reply: io.nats.client.Message = nc.request(subRequests, sr.toByteArray, 2L, TimeUnit.SECONDS)
      if (reply == null) {
        sub.inboxSub.get.unsubscribe()
        throw new IOException(MewNatsStreaming.ERR_SUB_REQ_TIMEOUT)
      }
      val response: SubscriptionResponse =
        try {
          val resp: SubscriptionResponse = SubscriptionResponse.parseFrom(reply.getData)
          resp
        } catch {
          case e: InvalidProtocolBufferException =>
            sub.inboxSub.get.unsubscribe()
            throw e
        }
      if (!response.getError.isEmpty) {
        sub.inboxSub.get.unsubscribe()
        throw new IOException(response.getError)
      }
      sub.setAckInbox(response.getAckInbox)
    } finally {
      sub.wUnlock()
    }
    sub
  }

  @throws[IOException]
  @throws[InterruptedException]
  def publish(subject: String, data: Array[Byte], ackHandler: Option[MewAckHandler], ch: Option[BlockingQueue[String]]): String = {
    val a: AckClosure = createAckClosure(ackHandler.get, ch.get)

    this.lock()

    val (subj, guid, _, bytes, ackSubject, ackTimeout, pac) = try {
      Option(getNatsConnection()) match {
        case Some(_) =>
        case None => throw new IllegalStateException(com.scala.tesco.mewbase.clientNatsStreaming.MewNatsStreaming.ERR_CONNECTION_CLOSED)
      }

      val subj: String = String.format(s"$pubPrefix.$subject")
      val guid: String = NUID.nextGlobal()
      var pb: PubMsg.Builder = PubMsg.newBuilder()
        .setClientID(clientId)
        .setGuid(guid)
        .setSubject(subject)
      if (data != null) {
        pb = pb.setData(ByteString.copyFrom(data))
      }
      val pe: PubMsg = pb.build()
      val bytes: Array[Byte] = pe.toByteArray

      //Map ack to guid
      pubAckMap.put(guid, a)
      //snapshot
      val ackSubject: String = this.ackSubject
      val ackTimeout: Duration = newOpts.getAckTimeout
      val pac: BlockingQueue[PubAck] = pubAckChan
      (subj, guid, pe, bytes, ackSubject, ackTimeout, pac)
    } finally {
      this.unlock()
    }

    // Use the buffered channel to control the number of outstanding acks.
    try {
      pac.put(PubAck.getDefaultInstance)
    } catch {
      case _: InterruptedException =>
      //TODO: Reevaluate this
      //Eat this because you cant really do anything with it
    }
    try {
      nc.publish(subj, ackSubject, bytes, true)
    } catch {
      case e: IOException =>
        removeAck(guid)
        throw e
    }
    //Setup the timer for expiration
    this.lock()
    try {
      a.ackTask = createAckTimerTask(guid)
      ackTimer.schedule(a.ackTask, ackTimeout.toMillis)

    } finally {
      this.unlock()
    }
    guid

  }

  @throws[IOException]
  @throws[InterruptedException]
  override def publish(subject: String, data: Array[Byte]): Unit = {
    val ch: BlockingQueue[String] = createErrorChannel()
    val chandlher: Option[BlockingQueue[String]] = Option(ch)
    val none: Option[MewAckHandler] = Option(new MewAckHandler {
      override def onAck(s: String, e: Exception): Unit = {
        println("OnAck")
      }
    })
    publish(subject, data, none, chandlher)
    try {
      val err: String = ch.take()
      if (!ch.isEmpty) {
        throw new IOException(err)
      }
    } catch {
      case _: InterruptedException => //TODO ignore for now but re-evaluate this
    }
  }

  @throws[IOException]
  @throws[InterruptedException]
  override def publish(subject: String, data: Array[Byte], ackHandler: MewAckHandler): String = {
    val ch: BlockingQueue[String] = createErrorChannel()
    val chandlher: Option[BlockingQueue[String]] = Option(ch)
    val ackH: Option[MewAckHandler] = Option(ackHandler)
    publish(subject, data, ackH, chandlher)
  }

  def getClientId: String = clientId

  def newInBox: String = nc.newInbox()

  def createAckTimerTask(guid: String): TimerTask = {
    new TimerTask {
      override def run(): Unit = try {
        processAckTimeout(guid)
      } catch {
        case _: Exception =>
          //catch exception to prevent the timer to be close
          cancel()
      }
    }
  }

  def processAckTimeout(guid: String): Unit = {
    val ackClosure: AckClosure = removeAck(guid)
    ackClosure match {
      case null =>
      case _ => ackClosure.ah match {
        case null =>
        case _ => ackClosure.ah.onAck(guid, new TimeoutException(MewNatsStreaming.ERR_TIMEOUT))
      }
    }
  }

  def createAckClosure(ah: MewAckHandler, ch: BlockingQueue[String]) = new AckClosure(ah, ch)

  def createErrorChannel(): LinkedBlockingQueue[String] = new LinkedBlockingQueue[String]

  def processAck(msg: io.nats.client.Message): Unit = {
    val pa: PubAck =
      try {
        PubAck.parseFrom(msg.getData)
      } catch {
        case e: InvalidProtocolBufferException =>
          println("Protocol error: " + e.getStackTrace)
          null // Review this!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      }

    // Remove    REVIEW IF CORRECT CORRESPONDING TO JAVA (else if thing)
    val ackClosure: AckClosure = removeAck(pa.getGuid)
    Option(ackClosure) match {
      case Some(ackC) =>
        // Capture error if it exists
        val ackError: String = pa.getError
        val ex: Exception =
          Option(ackC.ah) match {
            case Some(_) if !ackError.isEmpty => new IOException(ackError)
            case _ => null
          }

        Option(ackC.ah) match {
          case Some(v) => v.onAck(pa.getGuid, ex)
          case None =>
            Option(ackC.ch) match {
              case Some(c) =>
                try {
                  c.put(ackError)
                } catch {
                  case _: InterruptedException => //ignore
                }
              case None =>
            }
        }
      case None =>
    }
  }

  def removeAck(guid: String): AckClosure = {
    this.lock()
    val (ackClosure: AckClosure, timerTask: TimerTask, pac: BlockingQueue[PubAck]) =
      try {
        val v1: AckClosure = pubAckMap.get(guid)
        val v2: Any = Option(v1) match {
          case Some(ackC) =>
            pubAckMap.remove(guid)
            ackC.ackTask
          case None =>
        }
        val v3: BlockingQueue[PubAck] = pubAckChan
        (v1, v2, v3)
      } finally {
        this.unlock()
      }

    // Cancel timer if needed
    Option(timerTask) match {
      case Some(t) => t.cancel()
      case None =>
    }
    // Remove from channel to unblock async publish
    Option(ackClosure) match {
      case Some(_) if pac.size() > 0 =>
        try {
          pac.take()
        } catch {
          case _: InterruptedException => //TODO: Ignore, but re-evaluate this
        }
      case None =>
    }
    ackClosure
  }

  override def getNatsConnection(): Connection = nc //Not returning Option[Connection] anymore

  def setNatsConnection(nc1: Connection): Unit = this.nc = nc1

  def lock(): Unit = mu.writeLock().lock()

  def unlock(): Unit = mu.writeLock().unlock()

  def rLock(): Unit = mu.readLock().lock()

  def rUnlock(): Unit = mu.readLock().unlock()

  def getAckSubscription: io.nats.client.Subscription = this.ackSubscription

  def getHbSubscription: io.nats.client.Subscription = this.hbSubscription

  // test injection setter/getters
  def setPubAckChan(ch: BlockingQueue[PubAck]): Unit = this.pubAckChan = ch

  def getPubAckChan: BlockingQueue[PubAck] = pubAckChan

  def setPubAckMap(map: util.HashMap[String, AckClosure]): Unit = this.pubAckMap = map

  def getPubAckMap: util.HashMap[String, AckClosure] = pubAckMap

  def setSubMap(map: util.HashMap[String, MewSubscriptionImpl]): Unit = this.subMap = map

  def getSubMap: util.HashMap[String, MewSubscriptionImpl] = subMap

  /**
    * Create a NATS connection if it doesn't exist
    */
  @throws[IOException]
  def createNatsConnection: Connection = {
    val v1: Option[Connection] = Option(getNatsConnection())
    v1 match {
      case None =>
        val c: Connection = if (newOpts.getNatsUrl != null) {
          ///???
          val natsOpts: Options = new io.nats.client.Options.Builder().name(clientId).build()
          Nats.connect(newOpts.getNatsUrl, natsOpts)
        } else {
          Nats.connect
        }
        ncOwned = true
        c
      case Some(n) => n
    }
  }

  @throws[IOException]
  @throws[InterruptedException]
  override def close(): Unit = {
    this.lock()
    try
      getNatsConnection() match {
        case null =>
        case _ =>
          val nc: io.nats.client.Connection = getNatsConnection()
          try {
            setNatsConnection(null)
            getAckSubscription match {
              case null =>
              case _ =>
                try {
                  getAckSubscription.unsubscribe()
                } catch {
                  case _: IOException => //ignore

                }
            }
            getHbSubscription match {
              case null =>
              case _ =>
                try {
                  getHbSubscription.unsubscribe()
                } catch {
                  case _: IOException => //ignore

                }
            }
            val req: CloseRequest = CloseRequest.newBuilder().setClientID(clientId).build()
            val bytes: Array[Byte] = req.toByteArray
            val reply: io.nats.client.Message = nc.request(closeRequests, bytes, newOpts.getConnectTimeout.toMillis)
            reply match {
              case null => throw new IOException(MewNatsStreaming.ERR_CLOSE_REQ_TIMEOUT)
              case _ =>
            }
            reply.getData match {
              case null =>
              case _ =>
                val cr: CloseResponse = CloseResponse.parseFrom(reply.getData)
                if (cr.getError.isEmpty) {
                } else {
                  throw new IOException(cr.getError)
                }
            }
          } finally {
            if (ncOwned) {
              try {
                nc.close()
              } catch {
                case _: Exception => //ignore
              }
            } else {

            }
          }
      }
    finally {
      this.unlock()
    }
  }


  def createSubscription(subject: String, qgroup: String, cb: MewMessageHandler, conn: MewStreamingConnectionImpl, opts: MewSubscriptionOptions): MewSubscriptionImpl =
    new MewSubscriptionImpl(subject, qgroup, cb, conn, opts)

  def processHeartBeat(msg: io.nats.client.Message): Unit = {
    // No payload assumed, just reply
    this.rLock()
    val nC: Option[Connection] = Option(nc)
    this.rUnlock()
    nC match {
      case Some(v) =>
        try {
          v.publish(msg.getReplyTo, null)
        } catch {
          case _: IOException =>
          // ignore exception; nothing we can do
        }
      case None =>
    }
  }

  def createSubscriptionRequest(sub: MewSubscriptionImpl): SubscriptionRequest = {
    val subOpts: MewSubscriptionOptions = sub.getOptions
    val srb: SubscriptionRequest.Builder = SubscriptionRequest.newBuilder()
    val clientId: String = sub.getConnection.getClientId
    val queue: String = sub.getQueue
    val subject: String = sub.getSubject

    srb.setClientID(clientId).setSubject(subject).setQGroup(if (queue == null) "" else queue)
      .setInbox(sub.getInbox)
      .setMaxInFlight(subOpts.getMaxInFlight)
      .setAckWaitInSecs(subOpts.getAckWait.getSeconds.toInt)

    val v1: StartPosition = subOpts.getStartAt
    v1 match {
      case First =>
      case LastReceived =>
      case SequenceStart =>
        srb.setStartSequence(subOpts.getStartSequence)
      case TimeDeltaStart =>
        val delta: Long = ChronoUnit.NANOS.between(subOpts.getStartTime, Instant.now())
        srb.setStartTimeDelta(delta)
      case UNRECOGNIZED =>
      case _ =>
    }
    srb.setStartPosition(subOpts.getStartAt)

    val durName: Option[String] = Option(subOpts.getDurableName)
    durName match {
      case Some(_) => srb.setDurableName(subOpts.getDurableName)
      case None =>
    }
    srb.build()
  }

  override def onMessage(message: io.nats.client.Message): Unit = processMsg(message)

  def createStanMessage(msgp: MsgProto): MewMessage = new MewMessage(Option(msgp))


  def processMsg(raw: io.nats.client.Message): Unit = {
    val stanMsg: Option[MewMessage] =
      try {
        val msgp: MsgProto = MsgProto.parseFrom(raw.getData)

        val rslt: MewMessage = createStanMessage(msgp)

        Option(rslt)
      } catch {
        case _: InvalidProtocolBufferException => // TODO: Ignore, but re-evaluate this
          None
      }
    //Lookup the subscription
    this.lock()
    val (nc: Option[Connection], isClosed: Boolean, sub: Option[MewSubscriptionImpl]) =
      try {
        val v1 = Option(getNatsConnection())
        val v2: Boolean = v1 match {
          case Some(_) => false
          case None => true
        }
        val v3 = Option(subMap.get(raw.getSubject))
        (v1, v2, v3)
      } finally {
        unlock()
      }

    // Check if sub is no longer valid or connection has been closed
    (sub, isClosed) match {
      case (None, _) =>
      case (_, true) =>
      case (_) =>
        // stanMsg.get.setSubsCription(sub)
        sub.get.rLock()
        val (cb: MewMessageHandler, ackSubject: String,
        isManualAck: Boolean, subsc: MewStreamingConnectionImpl) =
          try {
            val a1: MewMessageHandler = sub.get.getMessageHandler
            val a2: String = sub.get.getAckInbox
            val a3: Boolean = sub.get.getOptions.isManualAcks
            val a4: MewStreamingConnectionImpl = sub.get.getConnection
            (a1, a2, a3, a4)
          } finally {
            sub.get.rUnlock()
          }

        // Perform the callback
        (cb, subsc) match {
          case (a, _) => a.onMessage(stanMsg.get)
          case _ =>
        }

        // Process auto-ack
        if (!isManualAck) {
          val ack: Ack = Ack.newBuilder().setSubject(stanMsg.get.getSubject)
            .setSequence(stanMsg.get.getSequence).build()
          try {
            nc.get.publish(ackSubject, ack.toByteArray)
          } catch {
            case _: IOException =>
            // FIXME(dlc) - Async error handler? Retry?
            // This really won't happen since the publish is executing in the NATS thread.
          }
        }
    }
  }

  class AckClosure(val ah: MewAckHandler, val ch: BlockingQueue[String]) {
    var ackTask: TimerTask = _
  }


}
