package com.scala.tesco.mewbase.clientNatsStreaming

import java.io.IOException

import com.sun.corba.se.impl.orbutil.threadpool.TimeoutException
import io.nats.client.Connection


/**
  * Created by Ricardo Martins on 05/09/2017.
  */
trait MewStreamingConnection extends AutoCloseable{

  @throws[IOException]
  @throws[InterruptedException]
  def publish(subject: String, data: Array[Byte])

  @throws[IOException]
  @throws[InterruptedException]
  def publish(subject: String, data: Array[Byte], ah: MewAckHandler):String

  @throws[IOException]
  @throws[InterruptedException]
  def subscribe(subject: String, cb: MewMessageHandler): MewSubscription

  @throws[IOException]
  @throws[InterruptedException]
  def subscribe(subject: String, cb: MewMessageHandler, opts: MewSubscriptionOptions): MewSubscription

  @throws[IOException]
  @throws[InterruptedException]
  def subscribe(subject: String,queue: String, cb: MewMessageHandler): MewSubscription

  @throws[IOException]
  @throws[InterruptedException]
  def subscribe(subject: String,queue:String, cb: MewMessageHandler, opts: MewSubscriptionOptions): MewSubscription

  def getNatsConnection: Connection

  @throws[IOException]
  @throws[InterruptedException]
  @throws[TimeoutException]
  def close()
}
