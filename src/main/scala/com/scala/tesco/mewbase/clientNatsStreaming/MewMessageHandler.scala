package com.scala.tesco.mewbase.clientNatsStreaming

/**
  * Created by Tiago Filipe on 05/09/2017.
  */

/**
  * A MessageHandler object is used as a callback to receive asynchronously delivered messages.
  */
trait MewMessageHandler {
  /**
    * Passes a message to the handler.
    *
    * @param msg - the received Message that triggered the callback invocation.
    */
    def onMessage(msg: MewMessage): Unit
}
