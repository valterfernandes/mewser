package com.scala.tesco.mewbase.clientNatsStreaming

import java.io.IOException
import java.time.Instant

import io.nats.streaming.protobuf.{Ack, MsgProto}

/**
  * Created by Tiago Filipe on 01/09/2017.
  */
class MewMessage(msgp: Option[MsgProto] = None) {

  msgp match {
    case None => throw new NullPointerException("stan: MsgProto cannot be null")
    case Some(_) =>
  }

  var subject: String = msgp.get.getSubject
  var reply: String = msgp.get.getReply
  var data: Option[Array[Byte]] = Option(msgp.get.getData.toByteArray)
  // private MsgProto msgp; // MsgProto: Seq, Subject, Reply[opt], Data, Timestamp, CRC32[opt]
  val sequence: Long = msgp.get.getSequence
  val timestamp: Long = msgp.get.getTimestamp
  val redelivered: Boolean = msgp.get.getRedelivered
  val crc32: Int = msgp.get.getCRC32
  val immutable: Boolean = true

  var sub: MewSubscriptionImpl = _


  def getInstant: Instant = {
    val tsSeconds: Long = timestamp/ 1000000000L
    val tsNanos: Long = timestamp - (tsSeconds * 1000000000L)
    Instant.ofEpochSecond(tsSeconds).plusNanos(tsNanos)
  }


  def setSubscription(sub: MewSubscriptionImpl): Unit = this.sub = sub

  def getSubscription: MewSubscriptionImpl = sub

  def getSequence: Long = sequence

  def getSubject: String = subject

  def setSubject(subject: String): Unit ={
    if(immutable) throw new IllegalArgumentException("stan: message is immutable")
    this.subject = subject
  }

  def getReplyTo: String = reply

  def setReply(reply: String): Unit = {
    if(immutable) throw new IllegalArgumentException("stan: message is immutable")
    this.reply=reply
  }

  def getData: Option[Array[Byte]] = data

  def setData(data: Option[Array[Byte]]): Unit = {
    if(immutable) throw new IllegalArgumentException("stan: message is immutable")
    data match {
      case None => this.data = None
      case Some(_) => setData(data, 0, data.get.length)
    }
  }

  def setData(data : Option[Array[Byte]], offset: Int, length: Int): Unit = {
    if(immutable) throw new IllegalArgumentException("stan: message is immutable")
    val arr = new Array[Byte](length)
    this.data = Option(arr)
    Array.copy(data, offset, this.data, 0, length)
  }

  def getTimestamp: Long = timestamp

  def isRedelivered: Boolean =redelivered

  def getCrc32: Int = crc32

  @throws[IOException]
  def ack(): Unit = {
    sub.rLock()
    val (ackSubject: String, isManualAck: Boolean, sc: Option[MewStreamingConnectionImpl]) =
      try{
        val v1: String = sub.getAckInbox
        val v2: Boolean = sub.getOptions.isManualAcks
        val v3: Option[MewStreamingConnectionImpl] = Option(sub.getConnection)
        (v1, v2, v3)
      } finally {
        sub.rUnlock()
      }
    sc match {
      case None => throw  new IllegalStateException(MewNatsStreaming.ERR_BAD_SUBSCRIPTION)
      case Some(_) =>
    }

    if(!isManualAck) {
      throw new IllegalStateException(MewNatsStreaming.ERR_MANUAL_ACK)
    }

    // Ack here.
    val ack: Ack = Ack.newBuilder().setSubject(getSubject).setSequence(getSequence).build()
    sc.get.getNatsConnection().publish(ackSubject, ack.toByteArray)
  }

  override def toString: String = {
    val maxBytes = 32

    val bytes: Option[Array[Byte]] = getData
    val len: Int =
    bytes match {
      case Some(_) => bytes.get.length
      case None => 0
    }
    // SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY/MM/dd HH:mm:ss.SSS");
    val sb: StringBuilder = new StringBuilder
    // Date theDate = new Date(TimeUnit.NANOSECONDS.toMillis(getTimestamp()));
    sb.append(String.format(
      s"{Timestamp=$getTimestamp;Sequence=$getSequence;Redelivered=$isRedelivered;Subject=$getSubject;Reply=$getReplyTo;Payload=<"
    ))

    var i = 0
    while(i<len && i<maxBytes){
      sb.append(bytes.get(i).toChar)
      i+=1
    }

    val remainer: Int = len - maxBytes
    if(remainer > 0 ) {
      sb.append(String.format(s"$remainer more bytes"))
    }

    sb.append(">}")

    sb.toString()
  }

}
