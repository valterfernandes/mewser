package com.scala.tesco.mewbase.clientNatsStreaming

import java.io.IOException
import java.util.concurrent.locks.{ReadWriteLock, ReentrantReadWriteLock}

import io.nats.client.Connection
import io.nats.streaming.protobuf.{SubscriptionResponse, UnsubscribeRequest}


/**
  * Created by Ricardo Martins on 01/09/2017.
  */
object MewSubscriptionImpl{
  def DEFAULT_ACK_WAIT:Long = 30 * 1000
  def DEFAULT_MAX_IN_FLIGHT: Int = 1024
}
class MewSubscriptionImpl(subject: String, qgroup: String, cb: MewMessageHandler, sc: MewStreamingConnectionImpl, opts: MewSubscriptionOptions) extends MewSubscription {


  val opt: MewSubscriptionOptions = if(opts!=null) opts else {
    new MewSubscriptionOptions.Builder().build()
  }

  val inBox:Option[String] = Option(sc.newInBox)

  val rwlock: ReadWriteLock = new ReentrantReadWriteLock()

  var ackInbox:String = _
  var inboxSub: Option[io.nats.client.Subscription] = _

  def rLock():Unit = {
    rwlock.readLock().lock()
  }

  def rUnlock():Unit = {
    rwlock.readLock().unlock()
  }

  def wLock():Unit = {
    rwlock.writeLock().lock()
  }

  def wUnlock():Unit = {
    rwlock.writeLock().unlock()
  }

  def getAckInbox:String = ackInbox

  def getConnection:MewStreamingConnectionImpl = sc

  def getInbox:String = inBox.get

  def getMessageHandler: MewMessageHandler = cb

  override def getSubject: String = subject

  @throws[IOException]
  override def unsubscribe(): Unit = close(true)

  override def getQueue: String = qgroup

  override def getOptions: MewSubscriptionOptions = opt

  @throws[IOException]
  override def close(): Unit = if(sc != null)close(true)

  @throws[IOException]
  override def close(unsubscribe: Boolean): Unit = {
    wLock()
    val sc: MewStreamingConnectionImpl = try {
      val sc: MewStreamingConnectionImpl = this.sc
      inBox match {
        case None =>
          try {
              inboxSub.get.unsubscribe()
          } catch {
            case _: Exception =>
              //Silently ignore this, we can't do anything about it
          }
          inboxSub = None
        case Some(_) =>

      }
      sc
    } finally {
      wUnlock()
    }
    if (sc == null) throw new IllegalStateException(MewNatsStreaming.ERR_BAD_SUBSCRIPTION)

    sc.lock()
    val (nc: Connection, reqSubject: String) = try {
      if (sc == null) {
        throw new IllegalStateException(MewNatsStreaming.ERR_BAD_CONNECTION)
      }
      sc.subMap.remove(this.inBox.get)
      var reqSubject: String = sc.unsubRequests
      if (!unsubscribe) {
        reqSubject = sc.subCloseRequests
        if (reqSubject.isEmpty) {
          throw new IllegalStateException(MewNatsStreaming.ERR_NO_SERVER_SUPPORT)
        }
      }
      // Snapshot connection to avoid data race, since the connection may be
      // closing while we try to send the request
      val nc: Connection = sc.getNatsConnection()
      (nc, reqSubject)
    }finally {
      sc.unlock()
    }

    val usr:UnsubscribeRequest = UnsubscribeRequest.newBuilder().setClientID(sc.getClientId).setSubject(subject).setInbox(ackInbox).build()
    val bytes: Array[Byte] = usr.toByteArray

    val reply:io.nats.client.Message = try{
      val reply:io.nats.client.Message = nc.request(reqSubject, bytes, sc.newOpts.connectTimeout.toMillis)
      if(reply == null){
        if(unsubscribe)throw new IOException(MewNatsStreaming.ERR_UNSUB_REQ_TIMEOUT)
      throw new IOException(MewNatsStreaming.ERR_CLOSE_REQ_TIMEOUT)
      }
      reply
    }catch{
      case e:InterruptedException =>
        Thread.currentThread().interrupt()
        throw new IOException(e)
    }
    val response:SubscriptionResponse=SubscriptionResponse.parseFrom(reply.getData)
    if(!response.getError.isEmpty){
      throw new IOException(s"PFX ${response.getError}")
    }
  }

  def setAckInbox(ackIb: String):Unit = {
    ackInbox = ackIb
  }

}
