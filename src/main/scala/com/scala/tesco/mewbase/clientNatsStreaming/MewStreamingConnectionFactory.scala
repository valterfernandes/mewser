package com.scala.tesco.mewbase.clientNatsStreaming

import java.io.IOException
import java.time.Duration
import java.util.concurrent.TimeUnit

import io.nats.client.Connection

/**
  * Created by Ricardo Martins on 05/09/2017.
  */
class MewStreamingConnectionFactory(clusterid: String, clientid: String, server: String = MewNatsStreaming.DEFAULT_NATS_URL ) {
  var ackTimeout: Duration = Duration.ofMillis(MewSubscriptionImpl.DEFAULT_ACK_WAIT)
  var connectTimeout: Duration = Duration.ofSeconds(MewNatsStreaming.DEFAULT_CONNECT_WAIT)
  var discoverPrefix: String = MewNatsStreaming.DEFAULT_DISCOVER_PREFIX
  var maxPubAcksInFlight: Int = MewNatsStreaming.DEFAULT_MAX_PUB_ACKS_IN_FLIGHT
  var natsUrl: String = server //"nats://47.91.89.84:4222";
  var natsConn: Option[Connection] = None
  var clusterId: String = clusterid
  var clientId: String = clientid

  /**
    * Creates an active connection to a NATS Streaming server.
    *
    * return the StreamingConnection.
    * throws IOException          if a StreamingConnection cannot be established for some reason.
    * throws InterruptedException if the calling thread is interrupted before the connection can
    *                              be established
    */
  @throws[IOException]
  @throws[InterruptedException]
  def createConnection(): MewStreamingConnection = {
    new MewStreamingConnectionImpl(clusterId, clientId, options()).connect()
  }

  def options(): Option[MewOptions] = {
    val mo: MewOptions = natsConn match {
      case None =>
        new MewOptions.Builder()
        .connectWait(connectTimeout)
        .pubAckWait(ackTimeout)
        .discoverPrefix(discoverPrefix)
        .maxPubAcksInFlight(maxPubAcksInFlight)
        //.natsConnection(natsConn.get)
        .natsUrl(natsUrl)
        .build()
      case Some(_) =>
        new MewOptions.Builder()
        .connectWait(connectTimeout)
        .pubAckWait(ackTimeout)
        .discoverPrefix(discoverPrefix)
        .maxPubAcksInFlight(maxPubAcksInFlight)
        .natsConnection(natsConn.get)
        .natsUrl(natsUrl)
        .build()
    }

    Option(mo)
  }

  /**
    * Returns the ACK timeout.
    *
    * return the pubAckWait
    */
  def getAckTimeout: Duration = {
    ackTimeout
  }

  /**
    * Sets the ACK timeout duration.
    *
    * param ackTimeout the pubAckWait to set
    */
  def setAckTimeout(acktimeout: Duration): Unit = {
    this.ackTimeout = acktimeout
  }

  /**
    * Sets the ACK timeout in the specified time unit.
    *
    * param ackTimeout the pubAckWait to set
    * param unit       the time unit to set
    */
  def setAckTimeout(acktimeout: Long, unit: TimeUnit): Unit = {
    this.ackTimeout = Duration.ofMillis(unit.toMillis(acktimeout))
  }

  /**
    * Returns the connect timeout interval in milliseconds.
    *
    * return the connectWait
    */
  def getConnectionTimeout: Duration = {
    connectTimeout
  }

  /**
    * Sets the connect timeout duration.
    *
    * param connectTimeout the connectWait to set
    */
  def setConnectTimeout(connecttimeout:Duration): Unit = {
    this.connectTimeout = connecttimeout
  }

  /**
    * Sets the connect timeout in the specified time unit.
    *
    * param connectTimeout the connectWait to set
    * param unit           the time unit to set
    */
  def setConnectTimeout(connecttimeout: Long, unit: TimeUnit): Unit = {
    this.connectTimeout = Duration.ofMillis(unit.toMicros(connecttimeout))
  }

  /**
    * Returns the currently configured discover prefix string.
    *
    * return the discoverPrefix
    */
  def getDiscoverPrefix: String = {
    discoverPrefix
  }

  /**
    * Sets the discover prefix string that is used to establish a STAN session.
    *
    * param discoverPrefix the discoverPrefix to set
    */
  def setDiscoverPrefix(discoverprefix: String): Unit = {
    if(discoverprefix == null) throw new NullPointerException("stan: discoverPrefix must be non-null")

    this.discoverPrefix = discoverprefix
  }

  /**
    * Returns the maximum number of publish ACKs that may be in flight at any point in time.
    *
    * return the maxPubAcksInFlight
    */
  def getMaxPubAcksInFlight: Int = {
    maxPubAcksInFlight
  }

  /**
    * Sets the maximum number of publish ACKs that may be in flight at any point in time.
    *
    * param maxPubAcksInFlight the maxPubAcksInFlight to set
    */
  def setMaxPubAcksInFlight(maxpubacksinflight: Int): Unit = {
    if(maxpubacksinflight < 0) throw new IllegalArgumentException("stan: max publish acks in flight must be >= 0")
    this.maxPubAcksInFlight = maxpubacksinflight
  }

  /**
    * Returns the NATS connection URL.
    *
    * return the NATS connection URL
    */
  def getNatsUrl: String = {
    natsUrl
  }

  /**
    * Sets the NATS URL.
    *
    * param natsUrl the natsUrl to set
    */
  def setNatsUrls(natsurl: String):Unit = {
    if(natsurl == null) throw new NullPointerException("stan: NATS URL must be non-null")
    this.natsUrl= natsurl
  }

  /**
    * Returns the NATS StreamingConnection, if set.
    *
    * return the NATS StreamingConnection
    */
  def getNatsConnection: Connection = {
    natsConn.get
  }

  /**
    * Sets the NATS StreamingConnection.
    *
    * param natsConn the NATS connection to set
    */
  def setNatsConnection(natsconn: Connection): Unit = {
    this.natsConn = Option(natsconn)
  }

  /**
    * Returns the client ID of the current STAN session.
    *
    * return the client ID of the current STAN session
    */
  def getClientId: String = {
    clientId
  }

  /**
    * Sets the client ID for the current STAN session.
    *
    * param clientId the clientId to set
    */
  def setClientId(clientid: String): Unit = {
    if(clientid == null) throw new NullPointerException("stan: client ID must be non-null")
    this.clientId = clientid
  }

  /**
    * Returns the cluster ID of the current STAN session.
    *
    * return the clusterId
    */
  def setClusterId(clusterid: String): Unit = {
    if (clusterid == null) throw new NullPointerException("stan: cluster ID must be non-null")
    this.clusterId=clusterid
  }

}
