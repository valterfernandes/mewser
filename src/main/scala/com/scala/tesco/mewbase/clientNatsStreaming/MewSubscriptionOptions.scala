package com.scala.tesco.mewbase.clientNatsStreaming

import io.nats.streaming.protobuf.StartPosition
import com.scala.tesco.mewbase.clientNatsStreaming.MewSubscriptionOptions.Builder
import java.time.{Duration, Instant}
import java.util.Date
import java.util.concurrent.TimeUnit


/**
  * Created by Tiago Filipe on 05/09/2017.
  */

object MewSubscriptionOptions{

  /**
    * A Builder implementation for creating an immutable {code SubscriptionOptions} object.
    */
  class Builder extends Serializable{
    val serialVersionUID: Long = 1476017376308805473L

    var durableName: String = _

    var maxInFlight: Int = MewSubscriptionImpl.DEFAULT_MAX_IN_FLIGHT
    var ackWait: Duration = Duration.ofMillis(MewSubscriptionImpl.DEFAULT_ACK_WAIT)
    var startAt: StartPosition = StartPosition.NewOnly

    var startSequence: Long = _
    var startTime: Instant = _
    var manualAcks: Boolean = _
    var startTimeAsDate: Date = _

    /**
      * Sets the durable subscriber name for the subscription.
      *
      * @param durableName the name of the durable subscriber
      * @return this
      * @deprecated Use { @link #durableName(String)} instead
      */
    def setDurableName(durableName: String): Builder = durableNAME(durableName)

    /**
      * Sets the durable subscriber name for the subscription.
      *
      * @param durableName the name of the durable subscriber
      * @return this
      */
    def durableNAME(durableName: String): Builder = {
      this.durableName = durableName
      this
    }

    /**
      * Sets the maximum number of in-flight (unacknowledged) messages for the subscription.
      *
      * @param max maximum number of in-flight messages
      * @return this
      * @deprecated Use { @link #maxInFlight(int)} instead
      */
    def setMaxInFlight(max: Int): Builder = maxInFlight(max)

    /**
      * Sets the maximum number of in-flight (unacknowledged) messages for the subscription.
      *
      * @param max the maximum number of in-flight messages
      * @return this
      */
    def maxInFlight(max: Int): Builder = {
      this.maxInFlight = max
      this
    }

    /**
      * Sets the amount of time the subscription will wait for ACKs from the cluster.
      *
      * @param ackWait the amount of time the subscription will wait for an ACK from the cluster
      * @return this
      * @deprecated use { @link #ackWait(Duration)} instead
      */
    def setAckWait(ackWait: Duration): Builder = ackWAIT(ackWait)

    /**
      * Sets the amount of time the subscription will wait for ACKs from the cluster.
      *
      * @param ackWait the amount of time the subscription will wait for an ACK from the cluster
      * @param unit    the time unit
      * @return this
      * @deprecated use { @link #ackWait(long, TimeUnit)} instead
      */
    def setAckWAIT(ackWait: Long, unit: TimeUnit): Builder = ackWAIT(ackWait, unit)

    /**
      * Sets the amount of time the subscription will wait for ACKs from the cluster.
      *
      * @param ackWait the amount of time the subscription will wait for an ACK from the cluster
      * @return this
      */
    def ackWAIT(ackWait: Duration): Builder = {
      this.ackWait = ackWait
      this
    }

    /**
      * Sets the amount of time the subscription will wait for ACKs from the cluster.
      *
      * @param ackWait the amount of time the subscription will wait for an ACK from the cluster
      * @param unit    the time unit
      * @return this
      */
    def ackWAIT(ackWait: Long, unit: TimeUnit): Builder = {
      this.ackWait = Duration.ofMillis(unit.toMillis(ackWait))
      this
    }

    /**
      * Sets whether or not messages must be acknowledge individually by calling
      * {link Message#ack()}.
      *
      * @param manualAcks whether or not messages must be manually acknowledged
      * @return this
      * @deprecated use { @link #manualAcks()} instead
      */
    def setManualAcks(manualAcks: Boolean): Builder = manualACKS(manualAcks)

    /**
      * Sets whether or not messages must be acknowledge individually by calling
      * {link Message#ack()}.
      *
      * return this
      */
    def manualACKS(manualAcks: Boolean): Builder = {
      this.manualAcks = manualAcks
      this
    }

    /**
      * Specifies the sequence number from which to start receiving messages.
      *
      * @param seq the sequence number from which to start receiving messages
      * @return this
      */
    def startAtSequence(seq: Long): Builder = {
      this.startAt = StartPosition.SequenceStart
      this.startSequence = seq
      this
    }

    /**
      * Specifies the desired start time position using {code java.time.Instant}.
      *
      * @param start the desired start time position expressed as a { @code java.time.Instant}
      * @return this
      */
    def startAtTime(start: Instant): Builder = {
      this.startAt = StartPosition.TimeDeltaStart
      this.startTime = start
      this
    }

    /**
      * Specifies the desired delta start time position in the desired unit.
      *
      * @param ago  the historical time delta (from now) from which to start receiving messages
      * @param unit the time unit
      * @return this
      */
    def startAtTimeDelta(ago: Long, unit: TimeUnit): Builder = {
      this.startAt = StartPosition.TimeDeltaStart
      // this.startTime =
      // TimeUnit.MILLISECONDS.toNanos(System.currentTimeMillis() - unit.toMillis(ago));
      this.startTime = Instant.now().minusNanos(unit.toNanos(ago))
      this
    }

    /**
      * Specifies the desired delta start time as a {link java.time.Duration}.
      *
      * @param ago the historical time delta (from now) from which to start receiving messages
      * @return this
      */
    def startAtTimeDelta(ago: Duration): Builder = {
      this.startAt = StartPosition.TimeDeltaStart
      // this.startTime =
      // TimeUnit.MILLISECONDS.toNanos(System.currentTimeMillis() - unit.toMillis(ago));
      this.startTime = Instant.now()minusNanos ago.toNanos
      this
    }

    /**
      * Specifies that message delivery should start with the last (most recent) message stored
      * for this subject.
      *
      * @return this
      */
    def startWithLastReceived: Builder = {
      this.startAt = StartPosition.LastReceived
      this
    }

    /**
      * Specifies that message delivery should begin at the oldest available message for this
      * subject.
      *
      * @return this
      */
    def deliverAllAvailable: Builder = {
      this.startAt = StartPosition.First
      this
    }

    /**
      * Creates a {link SubscriptionOptions} instance based on the current configuration.
      *
      * @return the created { @link SubscriptionOptions} instance
      */
    def build(): MewSubscriptionOptions = new MewSubscriptionOptions(this)
  }
}

class MewSubscriptionOptions(builder: Builder) {

  // DurableName, if set will survive client restarts.
  val durableName: String = builder.durableName
  // Controls the number of messages the cluster will have inflight without an ACK.
  val maxInFlight: Int = builder.maxInFlight
  // Controls the time the cluster will wait for an ACK for a given message.
  val ackWait: Duration = builder.ackWait
  // StartPosition enum from proto.
  val startAt: StartPosition = builder.startAt
  // Optional start sequence number.
  val startSequence: Long = builder.startSequence
  // Optional start time in nanoseconds since the UNIX epoch.
  val startTime: Instant = builder.startTime
  // Option to do Manual Acks
  var manualAcks: Boolean = builder.manualAcks

  // Date startTimeAsDate;


  /**
    * Returns the name of the durable subscriber.
    *
    * @return the name of the durable subscriber
    */
  def getDurableName: String = durableName

  /**
    * Returns the maximum number of messages the cluster will send without an ACK.
    *
    * @return the maximum number of messages the cluster will send without an ACK
    */
  def getMaxInFlight: Int = maxInFlight

  /**
    * Returns the timeout for waiting for an ACK from the cluster's point of view for delivered
    * messages.
    *
    * @return the timeout for waiting for an ACK from the cluster's point of view for delivered
    *         messages
    */
  def getAckWait: Duration = ackWait

  /**
    * Returns the desired start position for the message stream.
    *
    * @return the desired start position for the message stream
    */
  def getStartAt: StartPosition = startAt

  /**
    * Returns the desired start sequence position.
    *
    * @return the desired start sequence position
    */
  def getStartSequence: Long = startSequence

  /**
    * Returns the desired start time position.
    *
    * @return the desired start time position
    */
  def getStartTime: Instant = startTime

  /**
    * Returns the desired start time position in the requested units.
    *
    * @param unit the unit of time
    * @return the desired start time position
    */
  def getStartTime(unit: TimeUnit): Long = {
    //FIXME use BigInteger representation
    var totalNanos: Long = TimeUnit.SECONDS.toNanos(startTime.getEpochSecond)
    totalNanos += startTime.getNano
    unit.convert(totalNanos, TimeUnit.NANOSECONDS)
  }

  /**
    * Returns whether or not messages for this subscription must be acknowledged individually by
    * calling {link Message#ack()}.
    *
    * @return whether or not manual acks are required for this subscription.
    */
  def isManualAcks: Boolean = manualAcks

}
