package com.scala.tesco.mewbase.clientNatsStreaming

import io.nats.client.Connection

import java.time.Duration

import com.scala.tesco.mewbase.clientNatsStreaming.MewOptions.Builder

/**
  * Created by Ricardo Martins on 31/08/2017.
  */
object  MewOptions{
  class Builder(options: Option[MewOptions] = None) extends Serializable {
    val serialVersionUID = 4774214916207501660L

    var natsUrls: String = options match {
      case None => MewNatsStreaming.DEFAULT_NATS_URL
      case Some(mo) => mo.natsUrl
    }
    var connectTimeout: Duration = options match {
      case None => Duration.ofSeconds(MewNatsStreaming.DEFAULT_CONNECT_WAIT)
      case Some(mo) => mo.connectTimeout
    }
    var ackTimeout: Duration = options match {
      case None => Duration.ofMillis(MewSubscriptionImpl.DEFAULT_ACK_WAIT)
      case Some(mo) => mo.ackTimeout
    }
    var discoverPrefix: String = options match {
      case None =>  MewNatsStreaming.DEFAULT_DISCOVER_PREFIX
      case Some(mo) => mo.discoverPrefix
    }
    var maxPubAcksInFlight: Int = options match {
      case None =>  MewNatsStreaming.DEFAULT_MAX_PUB_ACKS_IN_FLIGHT
      case Some(mo) => mo.maxPubAckInFlight
    }
    var natsConn: Option[Connection] = options match {
      case None => None
      case Some(mo) => mo.natsConnection
    }

    def pubAckWait(acktimeout: Duration): Builder = {
      this.ackTimeout = acktimeout
      this
    }

    def connectWait(connecttimeout: Duration): Builder = {
      this.connectTimeout = connecttimeout
      this
    }

    def discoverPrefix(discoverprefix: String): Builder = {
      this.discoverPrefix = discoverprefix
      this
    }

    def maxPubAcksInFlight(maxpubacksinflight:Int): Builder = {
      this.maxPubAcksInFlight = maxpubacksinflight
      this
    }

    def natsConnection(conn: Connection): Builder = {
      this.natsConn = Option(conn)
      this
    }
    def natsUrl(natsurl: String):Builder = {
      this.natsUrls = natsurl
      this
    }

    def build(): MewOptions = {
      new MewOptions(this)
    }

  }
}







class MewOptions(builder: Builder) {

  val natsUrl: String = builder.natsUrls
  val natsConnection: Option[Connection] = builder.natsConn
  val connectTimeout: Duration = builder.connectTimeout
  val ackTimeout: Duration = builder.ackTimeout
  val discoverPrefix: String = builder.discoverPrefix
  val maxPubAckInFlight: Int = builder.maxPubAcksInFlight


  def getNatsUrl:String = {
    natsUrl
  }

  def getNatsConn:Connection = natsConnection match {
    case None => null
    case Some(conn) => conn
  }

  def getConnectTimeout: Duration = {
    connectTimeout
  }

  def getAckTimeout:Duration = {
    ackTimeout
  }

  def getDiscoverPrefix: String = {
    discoverPrefix
  }

  def getMaxPubAckInFlight:Int = {
    maxPubAckInFlight
  }

}
