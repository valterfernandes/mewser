package com.scala.tesco.mewbase.clientNatsStreaming

import java.io.IOException

/**
  * Created by Ricardo Martins on 05/09/2017.
  */
trait MewSubscription extends AutoCloseable {

  def getSubject: String

  def getQueue: String

  @throws[IOException]
  def unsubscribe()

  @throws[IOException]
  def close()

  @throws[IOException]
  def close(unsubscribe: Boolean)

  def getOptions: MewSubscriptionOptions

}
