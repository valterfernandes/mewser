package com.scala.tesco.mewbase.clientNatsStreaming

/**
  * Created by Tiago Filipe on 05/09/2017.
  */

/**
  * A callback interface for handling NATS Streaming message acknowledgements.
  */
trait MewAckHandler {
  /**
    * This method is called when a message has been acknowledged by the STAN server, or if an error
    * has occurred during the publish operations. Processes the message acknowledgement (
    * {code NUID} ), along with any error that was encountered
    *
    * param nuid the message NUID
    * param ex   any exception that was encountered
    */
  def onAck(nuid: String, ex: Exception): Unit
}
