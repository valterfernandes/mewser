package com.scala.tesco.mewbase.clientNatsStreaming

import java.io.IOException

import com.scala.tesco.mewbase
import com.scala.tesco.mewbase.clientNatsStreaming

/**
  * Created by Ricardo Martins on 01/09/2017.
  */
object MewNatsStreaming {
  val DEFAULT_NATS_URL = io.nats.client.Nats.DEFAULT_URL
  val DEFAULT_CONNECT_WAIT = 2 // Seconds

  val DEFAULT_DISCOVER_PREFIX : String= "_STAN.discover"
  val DEFAULT_ACK_PREFIX: String = "_STAN.acks"
  val DEFAULT_MAX_PUB_ACKS_IN_FLIGHT:Int =10000 //16384
  val PFX: String = "stan: "
  val ERR_CONNECTION_REQ_TIMEOUT: String = PFX + "connect request timeout"
  val ERR_CLOSE_REQ_TIMEOUT: String = PFX + "close request timeout"
  val ERR_SUB_REQ_TIMEOUT: String = PFX + "subscribe request timeout"
  val ERR_UNSUB_REQ_TIMEOUT: String = PFX + "unsubscribe request timeout"
  val ERR_CONNECTION_CLOSED: String = PFX + "connection closed"
  val ERR_TIMEOUT: String = PFX + "publish ack timeout"
  val ERR_BAD_ACK: String = PFX + "malformed ack"
  val ERR_BAD_SUBSCRIPTION : String= PFX + "invalid subscription"
  val ERR_BAD_CONNECTION: String = PFX + "invalid connection"
  val ERR_MANUAL_ACK : String= PFX + "cannot manually ack in auto-ack mode"
  val ERR_NULL_MSG: String = PFX + "null message"
  val ERR_NO_SERVER_SUPPORT: String = PFX + "not supported by server"
  // Server errors
  val SERVER_ERR_INVALID_SUBJECT: String = PFX + "invalid subject"
  val SERVER_ERR_INVALID_SEQUENCE: String = PFX + "invalid start sequence"
  val SERVER_ERR_INVALID_TIME: String = PFX + "invalid start time"
  val SERVER_ERR_INVALID_SUB: String = PFX + "invalid subscription"
  val SERVER_ERR_INVALID_CLIENT: String = PFX + "clientID already registered"
  val SERVER_ERR_INVALID_ACK_WAIT: String = PFX + "invalid ack wait time, should be >= " + "1s"
  val SERVER_ERR_INVALID_CONN_REQ: String= PFX + "invalid connection request"
  val SERVER_ERR_INVALID_PUB_REQ: String = PFX + "invalid publish request"
  val SERVER_ERR_INVALID_SUB_REQ: String = PFX + "invalid subscription request"
  val SERVER_ERR_INVALID_UNSUB_REQ: String = PFX + "invalid unsubscribe request"
  val SERVER_ERR_INVALID_CLOSE_REQ: String = PFX + "invalid close request"
  val SERVER_ERR_DUP_DURABLE: String = PFX + "duplicate durable registration"
  val SERVER_ERR_INVALID_DURABLE_NAME: String = PFX + "durable name of a durable queue subscriber can't contain the character ':'"
  val SERVER_ERR_UNKNOWN_CLIENT: String = PFX + "unknown clientID"

  @throws[IOException]
  @throws[InterruptedException]
  def connect(cluster1: String, clientId: String): MewStreamingConnection = {
    connect(cluster1, clientId, defaultOptions())
  }

  @throws[IOException]
  @throws[InterruptedException]
  def connect(cluster1: String, clientId: String, opts: Option[MewOptions] =Some(new clientNatsStreaming.MewOptions.Builder().build())): MewStreamingConnection = {
  try{
    val conn:MewStreamingConnectionImpl = new MewStreamingConnectionImpl(cluster1, clientId, opts)
    conn.connect()
  }catch{
    case _:InterruptedException =>
      Thread.currentThread().interrupt()
      null
  }
  }

  def defaultOptions(): Option[MewOptions] = Option(new mewbase.clientNatsStreaming.MewOptions.Builder().build())
}
