name := "mewser"

version := "1.0"

scalaVersion := "2.12.1"




libraryDependencies ++= Seq(
  "org.scalatest"     %% "scalatest"   % "3.0.3" % Test withSources(),
  "junit"             %  "junit"       % "4.12"  % Test,
  "io.vertx" % "vertx-core" % "3.4.2",
  "de.undercouch" % "bson4jackson" % "2.7.0",
  //"org.slf4j" % "slf4j-jcl" % "1.2"
  "com.github.tyagihas" % "scala_nats_2.11" % "0.3.0",
  "javax.json" % "javax.json-api" % "1.1",
  "org.glassfish" % "json" % "1.1",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4",
  //"io.nats" % "java-nats-streaming" % "0.4.1",
  "io.netty" % "netty-buffer" % "4.1.16.Final",
  "io.netty" % "netty-codec" % "4.1.16.Final",
  "io.netty" % "netty-codec-dns" % "4.1.16.Final",
  "io.netty" % "netty-codec-http2" % "4.1.16.Final",
  "io.netty" % "netty-codec-http" % "4.1.16.Final",
  "io.netty" % "netty-codec-socks" % "4.1.16.Final",
  "io.netty" % "netty-common" % "4.1.16.Final",
  "io.netty" % "netty-handler" % "4.1.16.Final",
  "io.netty" % "netty-handler-proxy" % "4.1.16.Final",
  "io.netty" % "netty-resolver" % "4.1.16.Final",
  "io.netty" % "netty-resolver-dns" % "4.1.16.Final",
  "io.netty" % "netty-transport" % "4.1.16.Final"
)